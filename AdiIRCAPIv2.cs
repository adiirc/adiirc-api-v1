using System;
using System.Text;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = false)]
[assembly: ComVisible(true)]
[assembly: Guid("16d56dc8-25f6-42aa-b831-4eeac242dfbf")]
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]

namespace AdiIRCAPI
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// The window types for a IWindow
    /// </summary>
    public enum WindowType
    {
        /// <summary>
        /// Indicates the IWindow is a IServer window
        /// </summary>
        Server = 0,

        /// <summary>
        /// Indicates the IWindow is a IChannel window
        /// </summary>
        Channel = 1,

        /// <summary>
        /// Indicates the IWindow is a Iuser window
        /// </summary>
        Private = 2,

        /// <summary>
        /// Indicates the IWindow is a Chat window
        /// </summary>
        Chat = 3,

        /// <summary>
        /// Indicates the IWindow is a Tool window (Rawlog)
        /// </summary>
        Tool = 4,

        /// <summary>
        /// Indicates the IWindow is a ICustomWindow
        /// </summary>
        Custom = 5,

        /// <summary>
        /// Indicates the IWindow is a dock panel window
        /// </summary>
        Panel = 6,

        /// <summary>
        /// Indicates the IWindow is a Trebar Notify window (ignore this)
        /// </summary>
        Notify = 7
    }

    /// <summary>
    /// Used to determine the menu type during a OnMenu event
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// Indicates the OnMenu event has no type
        /// </summary>
        None = 0,

        /// <summary>
        /// Indicates the OnMenu event is for a Server window
        /// </summary>
        Server = 1,

        /// <summary>
        /// Indicates the OnMenu event is for a Channel window
        /// </summary>
        Channel = 2,

        /// <summary>
        /// Indicates the OnMenu event is for a Private window
        /// </summary>
        Private = 3,

        /// <summary>
        /// Indicates the OnMenu event is for a Custom window
        /// </summary>
        Custom = 4,

        /// <summary>
        /// Indicates the OnMenu event is for a Nicklist nick
        /// </summary>
        Nicklist = 5,

        /// <summary>
        /// Indicates the OnMenu event is for the Menubar
        /// </summary>
        Menubar = 6,

        /// <summary>
        /// Indicates the OnMenu event is for a Channel link
        /// </summary>
        ChannelLink = 7,

        /// <summary>
        /// Indicates the OnMenu event is for a link
        /// </summary>
        Link = 8
    }

    /// <summary>
    /// Delegate gets called when the user changes a options and the config file is reloaded.
    /// </summary>
    public delegate void OptionsChanged();

    /// <summary>
    /// Delegate gets called when a key is pressed down in a Editbox
    /// </summary>
    /// <param name="e">EditboxArgs</param>
    public delegate void EditboxKeyDown(EditboxArgs e);

    /// <summary>
    /// Delegate gets called when a menu is opened
    /// </summary>
    /// <param name="e">MenuEventArgs</param>
    public delegate void Menu(MenuEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer connects
    /// </summary>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void Connect(ConnectionEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer disconnects
    /// </summary>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void Disconnect(ConnectionEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer is registered
    /// </summary>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void Registered(ConnectionEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer retrives raw data
    /// </summary>
    /// <param name="e">DataArgs</param>
    public delegate void GetData(DataArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives a notice
    /// </summary>
    /// <param name="e">UserNoticeArgs</param>
    public delegate void UserNotice(NoticeArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel recives a notice
    /// </summary>
    /// <param name="e">UserNoticeArgs</param>>
    public delegate void ChannelNotice(NoticeArgs e);

    /// <summary>
    /// Delegate gets called when a IServer recives a notice
    /// </summary>
    /// <param name="e">UserNoticeArgs</param>
    public delegate void ServerNotice(NoticeArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel revices a message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void Message(MessageArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives a private message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void PrivateMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when you send a message to IChannel
    /// </summary>
    /// <param name="e">MessageArgs e</param>
    public delegate void UserMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel revices a mode
    /// </summary>
    /// <param name="e">ModeArgs</param>
    public delegate void Mode(ModeArgs e);

    /// <summary>
    /// Delegate gets called when your IUser revices a umode
    /// </summary>
    /// <param name="e">UserModeArgs</param>
    public delegate void UserMode(UserModeArgs e);

    /// <summary>
    /// Delegate gets called when a user on IChannel revices an invite
    /// </summary>
    /// <param name="e">InviteArgs</param>
    public delegate void Invite(InviteArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives an IChannel invite
    /// </summary>
    /// <param name="e">InviteArgs</param>
    public delegate void UserInvite(InviteArgs e);

    /// <summary>
    /// Delegate gets called when a IUser quits the IServer
    /// </summary>
    /// <param name="e">QuitArgs</param>
    public delegate void Quit(QuitArgs e);

    /// <summary>
    /// Delegate gets called when a IUser change nick
    /// </summary>
    /// <param name="e">NickArgs</param>
    public delegate void Nick(NickArgs e);

    /// <summary>
    /// Delegate gets called when a IUser changes topic in IChannel
    /// </summary>
    /// <param name="e">TopicArgs</param>
    public delegate void Topic(TopicArgs e);

    /// <summary>
    /// Delegate gets called when a IUser is kicked from IChannel
    /// </summary>
    /// <param name="e">KickArgs</param>
    public delegate void Kick(KickArgs e);

    /// <summary>
    /// Delegate gets called when a IUser joines a IChannel
    /// </summary>
    /// <param name="e">JoinArgs</param>
    public delegate void Join(JoinArgs e);

    /// <summary>
    /// Delegate gets called when a IUser parts a IChannel
    /// </summary>
    /// <param name="e">PartArgs</param>
    public delegate void Part(PartArgs e);

    /// <summary>
    /// Delegate gets called when your user types a hooked command
    /// </summary>
    /// <param name="e">Command</param>
    public delegate void Command(CommandArgs e);

    /// <summary>
    /// Delegate gets called when the command is called as a $identifier in a script
    /// </summary>
    /// <param name="e">Command</param>
    public delegate void Identifier(CommandArgs e);

    /// <summary>
    /// Delegate gets called when user sends data to the IServer
    /// </summary>
    /// <param name="e">DataArgs</param>
    public delegate void SendData(DataArgs e);

    /// <summary>
    /// Delegate gets called when any raw data is recieved to the IServer.
    /// You can modify the byte array args, AdiIRC will then parse this array with the proper encoding.
    /// If the array is set to null or zero bytes, the raw message is ignored by AdiIRC.
    /// </summary>
    /// <param name="e">RawDataArgs</param>
    public delegate void RawData(RawDataArgs e);

    /// <summary>
    /// Return data to the delegates stating whether the program should ignore none, ignore text output, or ignore everything
    /// Use with caution
    /// </summary>
    public enum EatData
    {
        /// <summary>
        /// Hide no part of the event.
        /// </summary>
        EatNone = 0,
        /// <summary>
        /// Hide the text output from this event.
        /// </summary>
        EatText = 1,
        /// <summary>
        /// Hide the entire event.
        /// </summary>
        EatAll = 2
    }

    /// <summary>
    /// Overall plugin manager
    /// </summary>
    public interface IPluginHost
    {
        /// <summary>
        /// Delegate gets called when the user changes a options and the config file is reloaded.
        /// </summary>
        event OptionsChanged OnOptionsChanged;

        /// <summary>
        /// Delegate gets called when a key is pressed down in a Editbox
        /// </summary>
        event EditboxKeyDown OnEditboxKeyDown;

        /// <summary>
        /// Delegate gets called when a menu is opened
        /// </summary>
        event Menu OnMenu;

        /// <summary>
        /// Delegate gets called when a IServer connects
        /// </summary>
        event Connect OnConnect;

        /// <summary>
        /// Delegate gets called when a IServer disconnects
        /// </summary>
        event Disconnect OnDisconnect;

        /// <summary>
        /// Delegate gets called when a IServer is registered
        /// </summary>
        event Registered OnRegistered;

        /// <summary>
        /// Delegate gets called when a IServer retrives raw data
        /// </summary>
        event GetData OnGetData;

        /// <summary>
        /// Delegate gets called when a IChannel recives a notice
        /// </summary>
        event ChannelNotice OnChannelNotice;

        /// <summary>
        /// Delegate gets called when a IServer recives a notice
        /// </summary>
        event ServerNotice OnServerNotice;

        /// <summary>
        /// Delegate gets called when a IUser parts a IChannel
        /// </summary>
        event Part OnPart;

        /// <summary>
        /// Delegate gets called when a IChannel revices a message
        /// </summary>
        event Message OnMessage;

        /// <summary>
        /// Delegate gets called when your IUser recives a private message
        /// </summary>
        event PrivateMessage OnPrivateMessage;

        /// <summary>
        /// Delegate gets called when your IUser send a message to object
        /// object can be either IChannel or IUser
        /// </summary>
        event UserMessage OnUserMessage;

        /// <summary>
        /// Delegate gets called when a IChannel revices a mode
        /// </summary>
        event Mode OnMode;

        /// <summary>
        /// Delegate gets called when your IUser revices a umode
        /// </summary>
        event UserMode OnUserMode;

        /// <summary>
        /// Delegate gets called when a user on IChannel revices an invite
        /// </summary>
        event Invite OnInvite;

        /// <summary>
        /// Delegate gets called when your IUser recives an IChannel invite
        /// </summary>
        event UserInvite OnUserInvite;

        /// <summary>
        /// Delegate gets called when a IUser quits the IServer
        /// </summary>
        event Quit OnQuit;

        /// <summary>
        /// Delegate gets called when a IUser change nick
        /// </summary>
        event Nick OnNick;

        /// <summary>
        /// Delegate gets called when a IUser changes topic in IChannel
        /// </summary>
        event Topic OnTopic;

        /// <summary>
        /// Delegate gets called when a IUser is kicked from IChannel
        /// </summary>
        event Kick OnKick;

        /// <summary>
        /// Delegate gets called when a IUser joines a IChannel
        /// </summary>
        event Join OnJoin;

        /// <summary>
        /// Delegate gets called when your user types a command
        /// </summary>
        event Command OnCommand;

        /// <summary>
        /// Delegate gets called when the command is called as a $identifier in a script
        /// </summary>
        event Identifier OnIdentifier;

        /// <summary>
        /// Delegate gets called when user sends data to the IServer
        /// </summary>
        event SendData OnSendData;

        /// <summary>
        /// Sends raw bytes to the IServer, bypasssing any encodings, AdiIRC is not aware when this messages are sent.
        /// </summary>
        event RawData OnRawData;

        /// <summary>
        /// Returns the main window handle
        /// </summary>
        IntPtr MainWindowHandle { get; }

        /// <summary>
        /// Returns the current active IWindow
        /// </summary>
        IWindow ActiveIWindow { get; }

        /// <summary>
        /// IMessagesOptions
        /// </summary>
        IMessagesOptions MessagesOptions { get; }

        /// <summary>
        /// IEditboxOptions
        /// </summary>
        IEditboxOptions EditboxOptions { get; }

        /// <summary>
        /// Returns a list of global variables which are saved across sessions
        /// </summary>
        ICollection GetVariables { get; }

        /// <summary>
        /// Evaluates identifiers in a script line
        /// </summary>
        /// <param name="window">Window/server to evaluate on</param>
        /// <param name="text">Text to evaluate</param>
        /// <param name="parameters">Optional parameters to use for $1-</param>
        /// <returns>The evaluated text</returns>
        string Evaluate(IWindow window, string text, string parameters);

        /// <summary>
        /// Use this to show information to the user. Shows in the specified window of any type.
        /// Returns if the message was shown
        /// </summary>
        /// <param name="window">Object</param>
        /// <param name="message">String</param>
        /// <returns>True/False depending if the notify was successful</returns>
        bool NotifyUser(IWindow window, string message);

        /// <summary>
        /// Use this to add /commands, you can then suscribe to OnCommand to get data from this command
        /// </summary>
        /// <param name="plugin">IPlugin</param>
        /// <param name="command">String</param>
        /// <param name="helpSyntax">String</param>
        /// <param name="description">String</param>
        /// <returns>True/False depending if hooking the command was successful</returns>
        bool HookCommand(IPlugin plugin, string command, string helpSyntax, string description);

        /// <summary>
        /// Remove a /command, you have defined.
        /// </summary>
        /// <param name="plugin">IPlugin</param>
        /// <param name="command">String</param>
        /// <returns>True/False depending if unhooking the command was successful</returns>
        bool UnHookCommand(IPlugin plugin, string command);

        /// <summary>
        /// Send a /command to a Window, window can be IServer, IChannel or IUser
        /// </summary>
        /// <param name="window">Object</param>
        /// <param name="command">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        bool SendCommand(IWindow window, string command);

        /// <summary>
        /// Get a list of IServers
        /// </summary>
        ICollection GetServers
        {
            get;
        }

        /// <summary>
        /// Get a list of IWindows
        /// </summary>
        ICollection GetWindows { get; }

        /// <summary>
        /// Returns the AdiIRC config folder.
        /// </summary>
        string ConfigFolder { get; }

        /// <summary>
        /// Returns the AdiIRC program folder.
        /// </summary>
        string ProgramFolder { get; }

        /// <summary>
        /// Returns System Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        string SystemUptime(int type);

        /// <summary>
        /// Returns AdiIRC Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        string Uptime(int type);
    }

    /// <summary>
    /// Inherit from this class in your plugin, e.g class MyPlugin : IPlugin
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Returns current plugin manager
        /// </summary>
        IPluginHost Host { get; set; }

        /// <summary>
        /// Returns ITools reference
        /// </summary>
        ITools Tools { get; set; }

        /// <summary>
        /// Get/Set plugin name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Get/Set plugin description
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Get/Set your name/nick
        /// </summary>
        string Author { get; }

        /// <summary>
        /// Get/Set plugin version
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Your email for support, optional, set to "" if you dont want to provide a email.
        /// </summary>
        string Email { get; }

        /// <summary>
        /// Called when plugin is loaded
        /// </summary>
        void Initialize();

        /// <summary>
        /// Called when plugin is unloaded
        /// </summary>
        void Dispose();
    }

    /// <summary>
    /// Represents a server object
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// Get the IServer name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// returns the IWindow associated with this IServer
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get the IServer network name
        /// </summary>
        string Network { get; }

        /// <summary>
        /// Get the IServer hostname
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Get the IServer target hostname entered in the Serverlist or /server
        /// </summary>
        string HostTarget { get; }

        /// <summary>
        /// Get the IServer IP address
        /// </summary>
        IPAddress HostIP { get; }

        /// <summary>
        /// Get the external Ip discovered from this IServer
        /// </summary>
        string ExternalIp { get; }

        /// <summary>
        /// Get the external host discovered from this IServer
        /// </summary>
        string ExternalHost { get; }

        /// <summary>
        /// Get the IServer port
        /// </summary>
        int Port { get; }

        /// <summary>
        /// Get whether IServer is using SSL secure connection
        /// </summary>
        bool IsUsingSSL { get; }

        /// <summary>
        /// Get the encoding used on this IServer
        /// </summary>
        Encoding Encoding { get; }

        /// <summary>
        /// Get current nick
        /// </summary>
        string Nick { get; }

        /// <summary>
        /// Get prefered nick
        /// </summary>
        string PrefNick { get; }

        /// <summary>
        /// Get prefered alternate nick
        /// </summary>
        string PrefNick2 { get; }

        /// <summary>
        /// Get usernick
        /// </summary>
        string UserNick { get; }

        /// <summary>
        /// Get realname
        /// </summary>
        string RealName { get; }

        /// <summary>
        /// Get supported userprefix characters on this IServer
        /// </summary>
        string UserPrefix { get; }

        /// <summary>
        /// Get supported userprefix letters on this IServer
        /// </summary>
        string UserPrefixChars { get; }

        /// <summary>
        /// Get supported channelprefixes on this IServer
        /// </summary>
        string ChannelPrefix { get; }

        /// <summary>
        /// Get server lag
        /// </summary>
        TimeSpan Lag { get; }

        /// <summary>
        /// Returns the IEditbox for the IServer window (can be null)
        /// </summary>
        IEditbox Editbox { get; }

        /// <summary>
        /// Returns the custom default BanType on this IServer, if any
        /// </summary>
        string BanType { get; }

        /// <summary>
        /// Returns the connection id on this IServer
        /// </summary>
        string ConnectionId { get; }

        /// <summary>
        /// Returns number of modes which can be in one MODE request on this IServer
        /// </summary>
        int ModesLength { get; }

        /// <summary>
        /// Returns the available channel modes on this IServer (retrieved from 005 chanmodes=)
        /// </summary>
        string ChannelModes { get; }

        /// <summary>
        /// Returns the available channel modes which can have an argument on this IServer (retrieved from 005 chanmodes=)
        /// </summary>
        ICollection ChannelModesWithArgs { get; }

        /// <summary>
        /// Returns the available channel modes which must have an argument on this IServer (retrieved from 005 chanmodes=)
        /// </summary>
        ICollection ChannelModesWithRequiredArgs { get; }

        /// <summary>
        /// Max topic length on this IServer (retrieved from 005)
        /// </summary>
        int TopicLength { get; }

        /// <summary>
        /// Max ban list count on this IServer (retrieved from 005)
        /// </summary>
        int BanLength { get; }

        /// <summary>
        /// Max except list count on this IServer (retrieved from 005)
        /// </summary>
        int ExceptLength { get; }

        /// <summary>
        /// Max invite list count on this IServer (retrieved from 005)
        /// </summary>
        int InviteLength { get; }

        /// <summary>
        /// Max quiet list count on this IServer (retrieved from 005)
        /// </summary>
        int QuietLength { get; }

        /// <summary>
        /// Returns whether ia away on this IServer
        /// </summary>
        bool Away { get; }

        /// <summary>
        /// Returns the away message on this IServer
        /// </summary>
        string AwayMessage { get; }

        /// <summary>
        /// Returns the time user went away on this IServer (UTC time)
        /// </summary>
        DateTime AwayTime { get; }

        /// <summary>
        /// Returns the time user connected to this IServer (UTC time)
        /// </summary>
        DateTime ConnectedAt { get; }

        /// <summary>
        /// Returns the time user last sent a message to this IServer (UTC time)
        /// </summary>
        DateTime IdleTime { get; }

        /// <summary>
        /// Returns whether the last connect attempt was a success
        /// </summary>
        bool ConnectionSuccess { get; }

        /// <summary>
        /// Returns whether the IServer is connected
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Returns whether the IServer is connecting
        /// </summary>
        bool IsConnecting { get; }

        /// <summary>
        /// Returns whether the IServer is re-connecting
        /// </summary>
        bool IsReconnecting { get; }

        /// <summary>
        /// Returns whether the user is logging on IServer
        /// </summary>
        bool IsLoggingOn { get; }

        /// <summary>
        /// Returns whether the user is logged on to the IServer
        /// </summary>
        bool IsRegistered { get; }

        /// <summary>
        /// Returns whether the last connection attempt failed on the IServer
        /// </summary>
        bool ConnectError { get; }

        /// <summary>
        /// Returns whether the error type of the last connections error (0 = no error)
        /// </summary>
        int Error { get; }

        /// <summary>
        /// Returns whether the the IServer uses the global or custom quit message
        /// </summary>
        bool UseGlobalQuitMessage { get; }

        /// <summary>
        /// Returns custom QuitMessage on this IServer, if enabled
        /// </summary>
        string QuitMessage { get; }

        /// <summary>
        /// Returns the last QuitMessage sent on this IServer
        /// </summary>
        string LastQuitMessage { get; }

        /// <summary>
        /// Returns the loginmethod for this IServer
        /// </summary>
        int LoginMethod { get; }

        /// <summary>
        /// Returns whether to use network label on this IServer
        /// </summary>
        bool UseNetworkLabel { get; }

        /// <summary>
        /// Network label for this IServer
        /// </summary>
        string NetworkLabel { get; }

        /// <summary>
        /// Returns number of seconds to delay joining channels on this IServer
        /// </summary>
        int DelayJoin { get; }

        /// <summary>
        /// Returns number of seconds to delay between each channel join on this IServer
        /// </summary>
        int DelayBetweenJoins { get; }

        /// <summary>
        /// Returs or set whether to use RegainNick on this IServer
        /// </summary>
        bool RegainNick { get; set; }

        /// <summary>
        /// Sends raw data to the IServer
        /// </summary>
        /// <param name="data">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        bool SendRaw(string data);

        /// <summary>
        /// Sends raw data to the client IServer, use this to fake messages
        /// E.g when you want to replace something in a PRIVMSG
        /// </summary>
        /// <param name="data">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        bool SendFakeRaw(string data);

        /// <summary>
        /// Finds the matching IChannel.
        /// </summary>
        /// <param name="name">String</param>
        /// <returns>A IChannel object or null</returns>
        IChannel FindChannel(string name);

        /// <summary>
        /// Finds the matching User.
        /// </summary>
        /// <param name="nick">String</param>
        /// <returns>Aa IUser object, if no user is found, a fake IUser object is created (but not stored).</returns>
        IUser FindUser(string nick);

        /// <summary>
        /// Get a collection of IChannels in this IServer
        /// </summary>
        ICollection GetChannels
        {
            get;
        }

        /// <summary>
        /// Get a collection of IUsers in this IServer
        /// </summary>
        ICollection GetUsers
        {
            get;
        }

        /// <summary>
        /// Sends raw bytes to the IServer, bypasssing any encodings, AdiIRC is not aware when these messages are sent.
        /// </summary>
        /// <param name="data">Byte[]</param>
        /// <returns>True/False depending if the send was successful</returns>
        bool SendRawData(byte[] data);

        /// <summary>
        /// Returns Server Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        string Uptime(int type);

        /// <summary>
        /// Connect this IServer if it's not connected
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnect this IServer if it's connected
        /// </summary>
        void Disconnect();
    }

    /// <summary>
    /// Represents a channel object
    /// </summary>
    public interface IChannel
    {
        /// <summary>
        /// Get the IChannel name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// returns the IWindow associated with this IChannel
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get the IServer associated with this IChannel
        /// </summary>
        IServer Server { get; }

        /// <summary>
        /// Get the IChannel topic
        /// </summary>
        string Topic { get; }

        /// <summary>
        /// Get nick of whom set the current topic
        /// </summary>
        string TopicSetBy { get; }

        /// <summary>
        /// Get date of when the current topic was set
        /// </summary>
        string TopicSetAt { get; }

        /// <summary>
        /// Gets whether the IChannel is moderated
        /// </summary>
        bool IsModerated { get; }

        /// <summary>
        /// Gets whether the IChannel is invite only
        /// </summary>
        bool IsInviteOnly { get; }

        /// <summary>
        /// Gets whether the IChannel is private
        /// </summary>
        bool IsPrivate { get; }

        /// <summary>
        /// Gets whether the IChannel is secret
        /// </summary>
        bool IsSecret { get; }

        /// <summary>
        /// Gets whether only operators can set topic in IChannel
        /// </summary>
        bool OnlyOpsSetTopic { get; }

        /// <summary>
        /// Gets whether the IChannel can recive external messages
        /// </summary>
        bool NoExternalMsgs { get; }

        /// <summary>
        /// Gets the IChannel channellimit
        /// </summary>
        int ChannelLimit { get; }

        /// <summary>
        /// Gets the IChannel password
        /// </summary>
        string ChannelPassword { get; }

        /// <summary>
        /// Gets the IChannel mode
        /// </summary>
        string ChannelModes { get; }

        /// <summary>
        /// Gets a collection of users in the IChannel
        /// </summary>
        ICollection GetUsers
        {
            get;
        }

        /// <summary>
        /// Returns the IEditbox for the IChannel window (can be null)
        /// </summary>
        IEditbox Editbox { get; }

        /// <summary>
        /// Returns the IChannel ban list
        /// </summary>
        Collection<IChannelMask> BanList { get; }

        /// <summary>
        /// Returns the IChannel except list
        /// </summary>
        Collection<IChannelMask> ExceptList { get; }

        /// <summary>
        /// Returns the IChannel invite list
        /// </summary>
        Collection<IChannelMask> InviteList { get; }

        /// <summary>
        /// Returns the IChannel quiet list
        /// </summary>
        Collection<IChannelMask> QuietList { get; }

        /// <summary>
        /// Returns the IChannel channel modes with arguments
        /// </summary>
        SortedDictionary<string, string> ChannelModeArgs { get; }
    }

    /// <summary>
    /// Represents a user
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Get user nick
        /// </summary>
        string Nick { get; }

        /// <summary>
        /// returns the IWindow associated with this IUser
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get user ident
        /// </summary>
        string Ident { get; }

        /// <summary>
        /// Get user hostname
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Get user realname
        /// </summary>
        string RealName { get; }

        /// <summary>
        /// Get whether user is IChannel operator (@)
        /// </summary>
        bool IsOperator { get; }

        /// <summary>
        /// Get whether user is IChannel special operator (!)
        /// </summary>
        bool IsPOperator { get; }

        /// <summary>
        /// Get whether user is IChannel owner (&amp;)
        /// </summary>
        bool IsOwner { get; }

        /// <summary>
        /// Get whether user has IChannel voice (+)
        /// </summary>
        bool HasVoice { get; }

        /// <summary>
        /// Get whether user is IChannel half operator (%)
        /// </summary>
        bool IsHalfOperator { get; }

        /// <summary>
        /// Get whether user is away
        /// </summary>
        bool IsAway { get; }

        /// <summary>
        /// /// Returns the time user last sent a message to this IChannel (UTC time)
        /// </summary>
        DateTime IdleTime { get; }

        /// <summary>
        /// /// Returns the time the user joined the IChannel (UTC time)
        /// </summary>
        DateTime Joined { get; }

        /// <summary>
        /// /// Returns whether the user has spoken since joining the IChannel
        /// </summary>
        bool HasSpoken { get; }

        /// <summary>
        /// /// Returns the nick color to use for the nick on this IChannel (UTC time)
        /// </summary>
        int NickColor { get; }

        /// <summary>
        /// /// Returns the rgb nick color to use for the nick on this IChannel (UTC time)
        /// </summary>
        string RgbNickColor { get; }

        /// <summary>
        /// Retrive all channels this IUser is in
        /// </summary>
        ICollection GetChannels { get; }

        /// <summary>
        /// Gets or sets the nickcolor,
        /// you can use any color from 1 to 80 from the client colors,
        /// this will override any random nick color
        /// </summary>
        //int NickColor { get; set; }

        /// <summary>
        /// Returns the IEditbox for the IUser/private window (can be null)
        /// </summary>
        IEditbox Editbox { get; }
    }

    /// <summary>
    /// Represents a form window
    /// </summary>
    public interface IWindow
    {
        /// <summary>
        /// Return the name of the IWindow
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Return the window name of the IWindow
        /// </summary>
        string NiceName { get; }

        /// <summary>
        /// Returns the IWindow title text
        /// </summary>
        string Text { get; }

        /// <summary>Return the IWindow window handle</summary>
        IntPtr WindowHandle { get; }

        /// <summary>
        /// Returns whether the IWindow is a MDI child
        /// </summary>
        bool IsMdiChild { get; }

        /// <summary>
        /// Returns the IWindow window state
        /// </summary>
        FormWindowState WindowState { get; set; }

        /// <summary>
        /// Returns whether the IWindow is visible
        /// </summary>
        bool Visible { get; set; }

        /// <summary>
        /// Returns the name of the log file associated with this IWindow
        /// </summary>
        string LogFile { get; }

        /// <summary>
        /// Returns the WindowType of this IWindow
        /// </summary>
        WindowType WindowType { get; }

        /// <summary>
        /// Returns the WindowType of this IWindow in a numeric form
        /// </summary>
        int WindowTypeNum { get; }

        /// <summary>
        /// Returns the window id of this IWindow
        /// </summary>
        int WindowId { get; }

        /// <summary>
        /// Returns the IEditbox associated with this IWindow
        /// </summary>
        IEditbox Editbox { get; }

        /// <summary>
        /// Returns the ITextView associated with this IWindow
        /// </summary>
        ITextView TextView { get; }

        /// <summary>
        /// Returns number of unread messages in this IWindow
        /// </summary>
        int UnreadMessages { get; }

        /// <summary>
        /// Returns the IServer associated with this IWindow
        /// </summary>
        IServer Server { get; }

        /// <summary>
        /// Returns the IChannel associated with this IWindow
        /// </summary>
        IChannel Channel { get; }

        /// <summary>
        /// Returns the IUser associated with this IWindow
        /// </summary>
        IUser User { get; }

        /// <summary>
        /// Returns the ICustomWindow associated with this IWindow
        /// </summary>
        ICustomWindow CustomWindow { get; }
    }

    /// <summary>
    /// Represents a custom window
    /// </summary>
    public interface ICustomWindow
    {
        /// <summary>
        /// Retrive the custom window name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// returns the IWindow associated with this ICustomWindow
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Returns True if the custom window is a picture window, otherwise false
        /// </summary>
        bool PictureWindow { get; }

        /// <summary>
        /// Returns the IEditbox for the ICustomwindow (can be null)
        /// </summary>
        IEditbox Editbox { get; }
    }

    /// <summary>
    /// Represents a Editbox
    /// </summary>
    public interface IEditbox
    {
        /// <summary>
        /// Returns the IEditbox text
        /// </summary>
        string Text { get; set; }

        /// <summary>
        /// Returns the selection start position in the IEditbox
        /// </summary>
        int SelectionStart { get; set; }

        /// <summary>
        /// Returns the selection length in the IEditbox
        /// </summary>
        int SelectionLength { get; set; }

        /// <summary>
        /// Returns the selected text in the IEditbox
        /// </summary>
        string SelectedText { get; set; }

        /// <summary>
        /// Selects a substring of the Text
        /// </summary>
        /// <param name="start">The start position to select</param>
        /// <param name="length">The length to select</param>
        void Select(int start, int length);
    }

    /// <summary>
    /// Represents a TextView
    /// </summary>
    public interface ITextView
    {
        /// <summary>
        /// Returns whether time stamps is enabled for this ITextView buffer
        /// </summary>
        bool UseTimestamp { get; }

        /// <summary>
        /// Returns the maxmimum buffer for this ITextView
        /// </summary>
        int MaxBuffer { get; set; }

        /// <summary>
        /// Returns the maxmimum number the scrollbar can scroll to (wrapped lines)
        /// </summary>
        int MaxScrollbar { get; }

        /// <summary>
        /// Returns the current scrollbar position (wrapped lines)
        /// </summary>
        int ScrollbarPos { get; }

        /// <summary>
        /// Returns the !TextView buffer lines
        /// </summary>
        ICollection Lines { get; }

        /// <summary>
        /// Returns the Nth ITextView buffer line
        /// </summary>
        string GetLine(int line);

        /// <summary>
        /// Adds a line of text to the ITextView buffer
        /// Adding lines manually requires all formatting to be done manually as well, format = 
        /// timestamp + " " + ITools.NickColumChar + nick + ITools.NickColumChar + " " + text
        /// </summary>
        void Add(string text, int lineColor, string rgbLineColor);

        /// <summary>
        /// Inserts a line of text at the Nth line in the ITextView buffer
        /// Adding lines manually requires all formatting to be done manually as well, format = 
        /// timestamp + " " + ITools.NickColumChar + nick + ITools.NickColumChar + " " + text
        /// </summary>
        void Insert(string text, int line, int lineColor, string rgbLineColor);

        /// <summary>
        /// Replaces the Nth line in the ITextView buffer with a new text
        /// Adding lines manually requires all formatting to be done manually as well, format = 
        /// timestamp + " " + ITools.NickColumChar + nick + ITools.NickColumChar + " " + text
        /// </summary>
        void Replace(string text, int line, int lineColor, string rgbLineColor);

        /// <summary>
        /// Removes the Nth ITextView buffer line
        /// </summary>
        void Remove(int start, int end);

        /// <summary>
        /// Clears the ITextView buffer
        /// </summary>
        void Clear();

        /// <summary>
        /// Scrolls the scrollbar to the Nth (wrapped) line
        /// </summary>
        void ScrollTo(int line);

        /// <summary>
        /// Scrolls the scrollbar to the unread line marker
        /// </summary>
        void ScrollToUnreadLine();

        /// <summary>
        /// Scrolls the scrollbar to the start
        /// </summary>
        void ScrollHome();

        /// <summary>
        /// Scrolls the scrollbar to the end
        /// </summary>
        void ScrollEnd();

        /// <summary>
        /// Scrolls the scrollbar down 1 (wrapped line)
        /// </summary>
        void ScrollDown();

        /// <summary>
        /// Scrolls the scrollbar up 1 (wrapped line)
        /// </summary>
        void ScrollUp();

        /// <summary>
        /// Scrolls the scrollbar 1 page up
        /// </summary>
        void ScrollPageUp();

        /// <summary>
        /// Scrolls the scrollbar 1 page down
        /// </summary>
        void ScrollPageDown();
    }

    /// <summary>
    /// Various tools
    /// </summary>
    public interface ITools
    {
        /// <summary>
        /// Gets the bold character
        /// </summary>
        char BoldChar { get; }

        /// <summary>
        /// Gets the color character
        /// </summary>
        char ColorChar { get; }

        /// <summary>
        /// Gets the underline character
        /// </summary>
        char UnderLineChar { get; }

        /// <summary>
        /// Gets the action character
        /// </summary>
        char ActionChar { get; }

        /// <summary>
        /// Gets the nick column character used to determine where in the ITextView buffer the nick coulmn should be
        /// </summary>
        char NickColumnChar { get; }

        /// <summary>
        /// Gets the link character used to determine if the text in the ITextView buffer is a link
        /// </summary>
        char LinkChar { get; }

        /// <summary>
        /// Gets path sperator / or \ , for future use
        /// </summary>
        string PS { get; }

        /// <summary>
        /// Returns a string with colors stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripColors(string text);

        /// <summary>
        /// Returns a string with bold stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripBold(string text);

        /// <summary>
        /// Returns a string with italic stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripItalic(string text);

        /// <summary>
        /// Returns a string with underline stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripUnderline(string text);

        /// <summary>
        /// Use this to write to debug.txt in the main folder, returns true if the debug was written
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>True/False depending if the write was successful</returns>
        bool Debug(string text);

        /// <summary>
        /// Converts string to DateTime, see Time variables for possible syntax
        /// </summary>
        /// <param name="syntax">String</param>
        /// <param name="time">DateTime</param>
        /// <returns>A parsed string</returns>
        string ParseTime(string syntax, DateTime time);

        /// <summary>
        /// Converts unix timestamp to a DateTime
        /// </summary>
        /// <param name="time">Double</param>
        /// <returns>A Datetime value</returns>
        DateTime UnixTimeToDate(double time);

        /// <summary>
        /// Converts DateTime to unix timestamp
        /// </summary>
        /// <param name="time">DateTime</param>
        /// <returns>A unix timestamp value</returns>
        long DateToUnixTime(DateTime time);

        /// <summary>
        /// Checks if a string is numeric
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>True/False whether the string is a number</returns>
        bool IsNumeric(string text);

        /// <summary>
        /// Open ups a tool window
        /// </summary>
        /// <param name="height">int</param>
        /// <param name="width">int</param>
        /// <param name="title">String</param>
        /// <param name="text">String</param>
        void ToolWindow(int height, int width, string title, string text);

        /// <summary>
        /// Makes the text bold if Boldify is enabled
        /// </summary>
        /// <param name="text">Text to boldify</param>
        /// <returns>Boldified text</returns>
        string Boldify(string text);

        /// <summary>
        /// Splits a string by a string
        /// </summary>
        /// <param name="text">String</param>
        /// <param name="splitBy">String</param>
        /// <returns>A list of strings</returns>
        string[] SplitString(string text, string splitBy);

        /// <summary>
        /// Returns system information based on Syntax, see system variables for more information
        /// </summary>
        /// <param name="text">String</param>
        /// <param name="args">String</param>
        /// <returns>A oarsed string</returns>
        string ParseSysinfo(string text, string args);
    }

    /// <summary>
    /// Interface for returning Messages options
    /// </summary>
    public interface IMessagesOptions
    {
        /// <summary>
        /// Returns the nick prefix/syntax to use for regular messages
        /// </summary>
        string PrefixUser { get; set; }

        /// <summary>
        /// Returns the nick prefix/syntax to use for CTCP messages
        /// </summary>
        string PrefixCtcp { get; set; }

        /// <summary>
        /// Returns the nick prefix/syntax to use for emote messages
        /// </summary>
        string PrefixEmote { get; set; }

        /// <summary>
        /// Returns the nick prefix/syntax to use for system messages
        /// </summary>
        string PrefixSys { get; set; }

        /// <summary>
        /// Returns the time format to use for messages
        /// </summary>
        string TimeFormat { get; set; }

        /// <summary>
        /// Returns whether global timestamps is enabled or disabled
        /// </summary>
        bool UseTimestamp { get; set; }
    }

    /// <summary>
    /// Interface for returning Editbox options
    /// </summary>
    public interface IEditboxOptions
    {
        /// <summary>
        /// Text to add when tabbing nicks
        /// </summary>
        string TabCompleteSuffix { get; set; }

        /// <summary>
        /// Text to add when tabbing nicks and the nick is the first word in the IEditbox
        /// </summary>
        string TabCompleteSuffixFirstWord { get; set; }
    }

    /// <summary>
    /// Interface for ban/except/invite/quiet lists
    /// </summary>
    public interface IChannelMask
    {
        string Mask { get; }
        long SetAt { get; }
        string SetBy { get; }
    }

    /// <summary>
    /// RawDataArgs
    /// </summary>
    public class RawDataArgs : EventArgs
    {
        /// <summary>
        /// RawDataArgs
        /// </summary>
        /// <param name="server"></param>
        /// <param name="bytes"></param>
        public RawDataArgs(IServer server, byte[] bytes)
        {
            Server = server;
            Bytes = bytes;
        }

        /// <summary>
        /// The IServer of this event.
        /// </summary>
        public IServer Server { get; set; }

        /// <summary>
        /// The raw bytes of this event.
        /// </summary>
        public byte[] Bytes { get; set; }
    }

    public class MenuEventArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly MenuType menuType;
        private readonly string text;
        private readonly ToolStripItemCollection menuItems;
        private EatData eataData;

        public MenuEventArgs(IWindow window, IServer server, MenuType menuType, string text, ToolStripItemCollection menuItems, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.menuType = menuType;
            this.text = text;
            this.menuItems = menuItems;
            this.eataData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public MenuType MenuType { get { return this.menuType; } }

        public string Text { get { return this.text; } }

        public ToolStripItemCollection MenuItems { get { return this.menuItems; } }

        public EatData EatData { get { return this.eataData; } set { this.eataData = value; } }
    }

    public class ConnectionEventArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private EatData eatData;

        public ConnectionEventArgs(IWindow window, IServer server, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class DataArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly string data;
        private GetData getData;

        public DataArgs(IWindow window, IServer server, string data, GetData getData)
        {
            this.window = window;
            this.server = server;
            this.data = data;
            this.getData = getData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public string Data { get { return this.data; } }

        public GetData GetData { get { return this.getData; } set { this.getData = value; } }
    }

    public class NoticeArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly string notice;
        private EatData eatData;

        public NoticeArgs(IWindow window, IServer server, IChannel channel, IUser user, string notice, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.notice = notice;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public string Notice { get { return this.notice; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class CommandArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly string command;
        private EatData eatData;

        public CommandArgs(IWindow window, string command, EatData eatData)
        {
            this.window = window;
            this.command = command;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public string Command { get { return this.command; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class KickArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly IUser byUser;
        private readonly string kickReason;
        private EatData eatData;

        public KickArgs(IWindow window, IServer server, IChannel channel, IUser user, IUser byUser, string kickReason, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.byUser = byUser;
            this.kickReason = kickReason;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public IUser ByUser { get { return this.byUser; } }

        public string KickReason { get { return this.kickReason; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class JoinArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IChannel channel;
        private readonly IUser user;
        private EatData eatData;

        public JoinArgs(IWindow window, IChannel channel, IUser user, EatData eatData)
        {
            this.window = window;
            this.channel = channel;
            this.user = user;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class PartArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly string partMessage;
        private EatData eatData;

        public PartArgs(IWindow window, IChannel channel, IUser user, string partMessage, EatData eatData)
        {
            this.window = window;
            this.channel = channel;
            this.user = user;
            this.partMessage = partMessage;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public string PartMessage { get { return this.partMessage; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class TopicArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly string newTopic;
        private EatData eatData;

        public TopicArgs(IWindow window, IServer server, IChannel channel, IUser user, string newTopic, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.newTopic = newTopic;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public string NewTopic { get { return this.newTopic; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class NickArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IUser user;
        private readonly string newNick;

        public NickArgs(IWindow window, IServer server, IUser user, string newNick)
        {
            this.window = window;
            this.server = server;
            this.user = user;
            this.newNick = newNick;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IUser User { get { return this.user; } }

        public string NewNick { get { return this.newNick; } }
    }

    public class EditboxArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly IEditbox editbox;
        private readonly KeyEventArgs keyEventArgs;

        public EditboxArgs(IWindow window, IServer server, IChannel channel, IUser user, IEditbox editbox, KeyEventArgs keyEventArgs)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.editbox = editbox;
            this.keyEventArgs = keyEventArgs;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public IEditbox Editbox { get { return this.editbox; } }

        public KeyEventArgs KeyEventArgs { get { return this.keyEventArgs; } }
    }

    public class MessageArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly String message;
        private EatData eatData;

        public MessageArgs(IWindow window, IServer server, IChannel channel, IUser user, String message, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.message = message;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public String Message { get { return this.message; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class ModeArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly String mode;
        private EatData eatData;

        public ModeArgs(IWindow window, IServer server, IChannel channel, IUser user, String mode, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.mode = mode;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public String Mode { get { return this.mode; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class UserModeArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly String mode;
        private EatData eatData;

        public UserModeArgs(IWindow window, IServer server, String mode, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.mode = mode;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public String Mode { get { return this.mode; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class InviteArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private EatData eatData;

        public InviteArgs(IWindow window, IServer server, IChannel channel, IUser user, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

    public class QuitArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IUser user;
        private readonly String quitMessage;
        private EatData eatData;

        public QuitArgs(IWindow window, IServer server, IUser user, String quitMessage, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.user = user;
            this.quitMessage = quitMessage;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IUser User { get { return this.user; } }

        public String QuitMessage { get { return this.quitMessage; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }

}
