﻿using System;
using System.Text;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = false)]
[assembly: ComVisible(true)]
[assembly: Guid("16d56dc8-25f6-42aa-b831-4eeac242dfbf")]
[assembly: AssemblyVersion("1.8.0.0")]
[assembly: AssemblyFileVersion("1.8.0.0")]

namespace AdiIRCAPI
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// The window types for a IWindow
    /// </summary>
    public enum WindowType
    {
        /// <summary>
        /// Indicates the IWindow is a IServer window
        /// </summary>
        Server = 0,

        /// <summary>
        /// Indicates the IWindow is a IChannel window
        /// </summary>
        Channel = 1,

        /// <summary>
        /// Indicates the IWindow is a Iuser window
        /// </summary>
        Private = 2,

        /// <summary>
        /// Indicates the IWindow is a Chat window
        /// </summary>
        Chat = 3,

        /// <summary>
        /// Indicates the IWindow is a Tool window (Rawlog)
        /// </summary>
        Tool = 4,

        /// <summary>
        /// Indicates the IWindow is a ICustomWindow
        /// </summary>
        Custom = 5,

        /// <summary>
        /// Indicates the IWindow is a dock panel window
        /// </summary>
        Panel = 6,

        /// <summary>
        /// Indicates the IWindow is a Trebar Notify window (ignore this)
        /// </summary>
        Notify = 7
    }

    /// <summary>
    /// Used to determine the menu type during a OnMenu event
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// Indicates the OnMenu event has no type
        /// </summary>
        None = 0,

        /// <summary>
        /// Indicates the OnMenu event is for a Server window
        /// </summary>
        Server = 1,

        /// <summary>
        /// Indicates the OnMenu event is for a Channel window
        /// </summary>
        Channel = 2,

        /// <summary>
        /// Indicates the OnMenu event is for a Private window
        /// </summary>
        Private = 3,

        /// <summary>
        /// Indicates the OnMenu event is for a Custom window
        /// </summary>
        Custom = 4,

        /// <summary>
        /// Indicates the OnMenu event is for a Nicklist nick
        /// </summary>
        Nicklist = 5,

        /// <summary>
        /// Indicates the OnMenu event is for the Menubar
        /// </summary>
        Menubar = 6,

        /// <summary>
        /// Indicates the OnMenu event is for a Channel link
        /// </summary>
        ChannelLink = 7,

        /// <summary>
        /// Indicates the OnMenu event is for a link
        /// </summary>
        Link = 8
    }

    /// <summary>
    /// Delegate gets called when the user changes a options and the config file is reloaded.
    /// </summary>
    public delegate void OptionsChanged();

    /// <summary>
    /// Delegate gets called when a key is pressed down in a Editbox
    /// </summary>
    /// <param name="Server">IServer</param>
    public delegate void EditboxKeyDown(IServer Server, Object Window, IEditbox Editbox, KeyEventArgs KeyPressEventArgs);

    /// <summary>
    /// Delegate gets called when a menu is opened
    /// </summary>
    /// <param name="Window">Object</param>
    /// <param name="MenuType">MenuType</param>
    /// <param name="Text">String</param>
    /// <param name="MenuItems">ToolStripItemCollection</param>
    public delegate void Menu(IServer Server, Object Window, MenuType MenuType, String Text, ToolStripItemCollection MenuItems);

    /// <summary>
    /// Delegate gets called when a IServer connects
    /// </summary>
    /// <param name="Server">IServer</param>
    public delegate void Connect(IServer Server);

    /// <summary>
    /// Delegate gets called when a IServer disconnects
    /// </summary>
    /// <param name="Server">IServer</param>
    public delegate void Disconnect(IServer Server);

    /// <summary>
    /// Delegate gets called when a IServer is registered
    /// </summary>
    /// <param name="Server">IServer</param>
    public delegate void Registered(IServer Server);

    /// <summary>
    /// Delegate gets called when a IServer retrives raw data
    /// </summary>
    /// <param name="Server">IServer</param>
    public delegate void GetData(IServer Server, String Data, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser recives a notice
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="User">IUser</param>
    /// <param name="Notice">String</param>
    /// <param name="Return">EatData</param>
    public delegate void UserNotice(IServer Server, IUser User, String Notice, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IChannel recives a notice
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="Notice">String</param>
    /// <param name="Return">EatData</param>
    public delegate void ChannelNotice(IServer Server, IChannel Channel, IUser User, String Notice, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IServer recives a notice
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Notice">String</param>
    /// <param name="Return">EatData</param>
    public delegate void ServerNotice(IServer Server, String Notice, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IUser parts a IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="PartMessage">String</param>
    /// <param name="Return">EatData</param>
    public delegate void Part(IServer Server, IChannel Channel, IUser User, String PartMessage, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser parts a IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="PartMessage">String</param>
    /// <param name="Return">IServer</param>
    public delegate void UserPart(IServer Server, IChannel Channel, String PartMessage, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IChannel revices a message
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="Message">String</param>
    /// <param name="Return">EatData</param>
    public delegate void Message(IServer Server, IChannel Channel, IUser User, String Message, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser recives a private message
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="User">IUser</param>
    /// <param name="Message">String</param>
    /// <param name="Return">EatData</param>
    public delegate void PrivateMessage(IServer Server, IUser User, String Message, out EatData Return);

    /// <summary>
    /// Delegate gets called when you send a message to IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Window">Object</param>
    /// <param name="Message">String</param>
    /// <param name="Return">EatData</param>
    public delegate void UserMessage(IServer Server, Object Window, String Message, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IChannel revices a mode
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="Mode">String</param>
    /// <param name="Return">EatData</param>
    public delegate void Mode(IServer Server, IChannel Channel, IUser User, String Mode, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser revices a umode
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Mode">String</param>
    /// <param name="Return">EatData</param>
    public delegate void UserMode(IServer Server, String Mode, out EatData Return);

    /// <summary>
    /// Delegate gets called when a user on IChannel revices an invite
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="Return">EatData</param>
    public delegate void Invite(IServer Server, IChannel Channel, IUser User, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser recives an IChannel invite
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="Return">EatData</param>
    public delegate void UserInvite(IServer Server, IChannel Channel, IUser User, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IUser quits the IServer
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="User">IUser</param>
    /// <param name="Data">String</param>
    /// <param name="Return">Return</param>
    public delegate void Quit(IServer Server, IUser User, String Data, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser quits the IServer
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="QuitMessage">String</param>
    /// <param name="Return">EatData</param>
    public delegate void UserQuit(IServer Server, String QuitMessage, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IUser change nick
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="User">IUser</param>
    /// <param name="NewNick">String</param>
    /// <param name="Return">EatData</param>
    public delegate void Nick(IServer Server, IUser User, String NewNick, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser change nick
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="NewNick">String</param>
    /// <param name="Return">EatData</param>
    public delegate void UserNick(IServer Server, String NewNick, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IUser changes topic in IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="NewTopic">String</param>
    /// <param name="Return">EatData</param>
    public delegate void Topic(IServer Server, IChannel Channel, IUser User, String NewTopic, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IUser is kicked from IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="ByUser">IUser</param>
    /// <param name="KickReason">String</param>
    /// <param name="Return">EatData</param>
    public delegate void Kick(IServer Server, IChannel Channel, IUser User, IUser ByUser, String KickReason, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser is kicked from IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="KickReason">String</param>
    /// <param name="Return">EatData</param>
    public delegate void UserKick(IServer Server, IChannel Channel, IUser User, String KickReason, out EatData Return);

    /// <summary>
    /// Delegate gets called when a IUser joines a IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="User">IUser</param>
    /// <param name="Return">EatData</param>
    public delegate void Join(IServer Server, IChannel Channel, IUser User, out EatData Return);

    /// <summary>
    /// Delegate gets called when your IUser joines a IChannel
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Channel">IChannel</param>
    /// <param name="Return">EatData</param>
    public delegate void UserJoin(IServer Server, IChannel Channel, out EatData Return);

    /// <summary>
    /// Delegate gets called when your user types a command
    /// Window can be IServer (server window), IChannel (channel window), IUser (private window)
    /// </summary>
    /// <param name="Window">Object</param>
    /// <param name="Command">String</param>
    /// <param name="Args">String</param>
    public delegate void Command(Object Window, String Command, String Args);

    /// <summary>
    /// Delegate gets called when the command is called as a $identifier in a script
    /// Window can be IServer (server window), IChannel (channel window), IUser (private window)
    /// </summary>
    /// <param name="Window">Object</param>
    /// <param name="Command">String</param>
    /// <param name="Args">String</param>
    /// <param name="Return">String</param>
    public delegate void Identifier(Object Window, String Command, String[] Args, out String Return);

    /// <summary>
    /// Delegate gets called when user sends data to the IServer
    /// </summary>
    /// <param name="Server">IServer</param>
    /// <param name="Data">String</param>
    /// <param name="Return">EatData</param>
    public delegate void SendData(IServer Server, String Data, out EatData Return);

    /// <summary>
    /// Delegate gets called when any raw data is recieved to the IServer.
    /// You can modify the byte array args, AdiIRC will then parse this array with the proper encoding.
    /// If the array is set to null or zero bytes, the raw message is ignored by AdiIRC.
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">RawDataArgs</param>
    public delegate void RawData(object sender, RawDataArgs e);

    /// <summary>
    /// Return data to the delegates stating whether the program should ignore none, ignore text output, or ignore everything
    /// Use with caution
    /// </summary>
    public enum EatData
    {
        /// <summary>
        /// Hide no part of the event.
        /// </summary>
        EatNone = 0,
        /// <summary>
        /// Hide the text output from this event.
        /// </summary>
        EatText = 1,
        /// <summary>
        /// Hide the entire event.
        /// </summary>
        EatAll = 2
    }

    /// <summary>
    /// Overall plugin manager
    /// </summary>
    public interface IPluginHost
    {
        /// <summary>
        /// Delegate gets called when the user changes a options and the config file is reloaded.
        /// </summary>
        event OptionsChanged OnOptionsChanged;

        /// <summary>
        /// Delegate gets called when a key is pressed down in a Editbox
        /// </summary>
        event EditboxKeyDown OnEditboxKeyDown;

        /// <summary>
        /// Delegate gets called when a menu is opened
        /// </summary>
        event Menu OnMenu;

        /// <summary>
        /// Delegate gets called when a IServer connects
        /// </summary>
        event Connect OnConnect;

        /// <summary>
        /// Delegate gets called when a IServer disconnects
        /// </summary>
        event Disconnect OnDisconnect;

        /// <summary>
        /// Delegate gets called when a IServer is registered
        /// </summary>
        event Registered OnRegistered;

        /// <summary>
        /// Delegate gets called when a IServer retrives raw data
        /// </summary>
        event GetData OnGetData;

        /// <summary>
        /// Delegate gets called when your IUser recives a notice
        /// </summary>
        event UserNotice OnUserNotice;

        /// <summary>
        /// Delegate gets called when a IChannel recives a notice
        /// </summary>
        event ChannelNotice OnChannelNotice;

        /// <summary>
        /// Delegate gets called when a IServer recives a notice
        /// </summary>
        event ServerNotice OnServerNotice;

        /// <summary>
        /// Delegate gets called when a IUser parts a IChannel
        /// </summary>
        event Part OnPart;

        /// <summary>
        /// Delegate gets called when your IUser parts a IChannel
        /// </summary>
        event UserPart OnUserPart;

        /// <summary>
        /// Delegate gets called when a IChannel revices a message
        /// </summary>
        event Message OnMessage;

        /// <summary>
        /// Delegate gets called when your IUser recives a private message
        /// </summary>
        event PrivateMessage OnPrivateMessage;

        /// <summary>
        /// Delegate gets called when your IUser send a message to object
        /// object can be either IChannel or IUser
        /// </summary>
        event UserMessage OnUserMessage;

        /// <summary>
        /// Delegate gets called when a IChannel revices a mode
        /// </summary>
        event Mode OnMode;

        /// <summary>
        /// Delegate gets called when your IUser revices a umode
        /// </summary>
        event UserMode OnUserMode;

        /// <summary>
        /// Delegate gets called when a user on IChannel revices an invite
        /// </summary>
        event Invite OnInvite;

        /// <summary>
        /// Delegate gets called when your IUser recives an IChannel invite
        /// </summary>
        event UserInvite OnUserInvite;

        /// <summary>
        /// Delegate gets called when a IUser quits the IServer
        /// </summary>
        event Quit OnQuit;

        /// <summary>
        /// Delegate gets called when your IUser quits the IServer
        /// </summary>
        event UserQuit OnUserQuit;

        /// <summary>
        /// Delegate gets called when a IUser change nick
        /// </summary>
        event Nick OnNick;

        /// <summary>
        /// Delegate gets called when your IUser change nick
        /// </summary>
        event UserNick OnUserNick;

        /// <summary>
        /// Delegate gets called when a IUser changes topic in IChannel
        /// </summary>
        event Topic OnTopic;

        /// <summary>
        /// Delegate gets called when a IUser is kicked from IChannel
        /// </summary>
        event Kick OnKick;

        /// <summary>
        /// Delegate gets called when your IUser is kicked from IChannel
        /// </summary>
        event UserKick OnUserKick;

        /// <summary>
        /// Delegate gets called when a IUser joines a IChannel
        /// </summary>
        event Join OnJoin;

        /// <summary>
        /// Delegate gets called when your IUser joines a IChannel
        /// </summary>
        event UserJoin OnUserJoin;

        /// <summary>
        /// Delegate gets called when your user types a command
        /// Window can be IServer (server window), IChannel (channel window), IUser (private window)
        /// </summary>
        event Command OnCommand;

        /// <summary>
        /// Delegate gets called when the command is called as a $identifier in a script
        /// Window can be IServer (server window), IChannel (channel window), IUser (private window)
        /// </summary>
        event Identifier OnIdentifier;

        /// <summary>
        /// Delegate gets called when user sends data to the IServer
        /// </summary>
        event SendData OnSendData;

        /// <summary>
        /// Sends raw bytes to the IServer, bypasssing any encodings, AdiIRC is not aware when this messages are sent.
        /// </summary>
        event RawData OnRawData;

        /// <summary>
        /// Returns the main window handle
        /// </summary>
        IntPtr MainWindowHandle { get; }

        /// <summary>
        /// Returns the current active window
        /// </summary>
        [Obsolete("Method1 is deprecated, please use ActiveIWindow instead.")]
        Object ActiveWindow { get; }

        /// <summary>
        /// Returns the current active IWindow
        /// </summary>
        IWindow ActiveIWindow { get; }

        /// <summary>
        /// IMessagesOptions
        /// </summary>
        IMessagesOptions MessagesOptions { get; }

        /// <summary>
        /// IEditboxOptions
        /// </summary>
        IEditboxOptions EditboxOptions { get; }

        /// <summary>
        /// Returns a list of global variables which are saved across sessions
        /// </summary>
        ICollection GetVariables { get; }

        /// <summary>
        /// Evaluates identifiers in a script line
        /// </summary>
        /// <param name="Window">Window/server to evaluate on</param>
        /// <param name="Text">Text to evaluate</param>
        /// <param name="Parameters">Optional parameters to use for $1-</param>
        /// <returns>The evaluated text</returns>
        String Evaluate(Object Window, String Text, String Parameters);

        /// <summary>
        /// Use this to show information to the user. Shows in the specified window of any type.
        /// Returns if the message was shown
        /// </summary>
        /// <param name="Window">Object</param>
        /// <param name="Message">String</param>
        /// <returns>True/False depending if the notify was successful</returns>
        Boolean NotifyUser(Object Window, String Message);

        /// <summary>
        /// Use this to show information to the user. Shows in IServer window.
        /// Returns if the message was shown
        /// </summary>
        /// <param name="Server">IServer</param>
        /// <param name="Message">String</param>
        /// <returns>True/False depending if the notify was successful</returns>
        Boolean NotifyUser(IServer Server, String Message);

        /// <summary>
        /// Use this to show information to the user. Shows in IChannel window
        /// Returns if the message was shown
        /// </summary>
        /// <param name="Channel">IChannel</param>
        /// <param name="Message">String</param>
        /// <returns>True/False depending if the notify was successful</returns>
        Boolean NotifyUser(IChannel Channel, String Message);

        /// <summary>
        /// Use this to show information to the user. Shows in IUser window
        /// Returns if the message was shown
        /// </summary>
        /// <param name="User">IUser</param>
        /// <param name="Message">String</param>
        /// <returns>True/False depending if the notify was successful</returns>
        Boolean NotifyUser(IUser User, String Message);

        /// <summary>
        /// Use this to show information to the user. Show in currently active window
        /// </summary>
        /// <param name="Message">String</param>
        /// <returns>True/False depending if the notify was successful</returns>
        Boolean NotifyUser(String Message);

        /// <summary>
        /// Use this to add /commands, you can then suscribe to OnCommand to get data from this command
        /// </summary>
        /// <param name="Plugin">IPlugin</param>
        /// <param name="Command">String</param>
        /// <param name="HelpSyntax">String</param>
        /// <param name="Description">String</param>
        /// <returns>True/False depending if hooking the command was successful</returns>
        Boolean HookCommand(IPlugin Plugin, String Command, String HelpSyntax, String Description);

        /// <summary>
        /// Remove a /command, you have defined.
        /// </summary>
        /// <param name="Plugin">IPlugin</param>
        /// <param name="Command">String</param>
        /// <returns>True/False depending if unhooking the command was successful</returns>
        Boolean UnHookCommand(IPlugin Plugin, String Command);

        /// <summary>
        /// Sends raw data to the IServer
        /// </summary>
        /// <param name="Server">IServer</param>
        /// <param name="Data">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendRaw(IServer Server, String Data);

        /// <summary>
        /// Sends raw data to the client IServer, use this to fake messages
        /// E.g when you want to replace something in a PRIVMSG
        /// </summary>
        /// <param name="Server">IServer</param>
        /// <param name="Data">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendFakeRaw(IServer Server, String Data);

        /// <summary>
        /// Send a /command to a Window, window can be IServer, IChannel or IUser
        /// </summary>
        /// <param name="Window">Object</param>
        /// <param name="Command">String</param>
        /// <param name="Args">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendCommand(Object Window, String Command, String Args);

        /// <summary>
        /// Get a list of IServers
        /// </summary>
        ICollection GetServers
        {
            get;
        }

        /// <summary>
        /// Get a list of IWindows
        /// </summary>
        ICollection GetWindows { get; }

        /// <summary>
        /// Sends raw bytes to the IServer, bypasssing any encodings, AdiIRC is not aware when this messages are sent.
        /// </summary>
        /// <param name="Server">IServer</param>
        /// <param name="Data">Byte[]</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendRawData(IServer Server, Byte[] Data);

        /// <summary>
        /// Returns the AdiIRC config folder.
        /// </summary>
        String ConfigFolder { get; }

        /// <summary>
        /// Returns the AdiIRC program folder.
        /// </summary>
        String ProgramFolder { get; }

        /// <summary>
        /// Returns System Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        String SystemUptime(int type);

        /// <summary>
        /// Returns AdiIRC Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        String Uptime(int type);
    }

    /// <summary>
    /// Inherit from this class in your plugin, e.g class MyPlugin : IPlugin
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Returns current plugin manager
        /// </summary>
        IPluginHost Host { get; set; }

        /// <summary>
        /// Returns ITools reference
        /// </summary>
        ITools Tools { get; set; }

        /// <summary>
        /// Get/Set plugin name
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Get/Set plugin description
        /// </summary>
        String Description { get; }

        /// <summary>
        /// Get/Set your name/nick
        /// </summary>
        String Author { get; }

        /// <summary>
        /// Get/Set plugin version
        /// </summary>
        String Version { get; }

        /// <summary>
        /// Your email for support, optional, set to "" if you dont want to provide a email.
        /// </summary>
        String Email { get; }

        /// <summary>
        /// Called when plugin is loaded
        /// </summary>
        void Initialize();

        /// <summary>
        /// Called when plugin is unloaded
        /// </summary>
        void Dispose();
    }

    /// <summary>
    /// Represents a server object
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// Get the IServer name
        /// </summary>
        String Name { get; }

        /// <summary>
        /// returns the IWindow associated with this IServer
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get the IServer network name
        /// </summary>
        String Network { get; }

        /// <summary>
        /// Get the IServer hostname
        /// </summary>
        String Host { get; }

        /// <summary>
        /// Get the IServer target hostname entered in the Serverlist or /server
        /// </summary>
        String HostTarget { get; }

        /// <summary>
        /// Get the IServer IP address
        /// </summary>
        IPAddress HostIP { get; }

        /// <summary>
        /// Get the external Ip discovered from this IServer
        /// </summary>
        String ExternalIp { get; }

        /// <summary>
        /// Get the external host discovered from this IServer
        /// </summary>
        String ExternalHost { get; }

        /// <summary>
        /// Get the IServer port
        /// </summary>
        int Port { get; }

        /// <summary>
        /// Get whether IServer is using SSL secure connection
        /// </summary>
        Boolean IsUsingSSL { get; }

        /// <summary>
        /// Get the encoding used on this IServer
        /// </summary>
        Encoding Encoding { get; }

        /// <summary>
        /// Get current nick
        /// </summary>
        String Nick { get; }

        /// <summary>
        /// Get prefered nick
        /// </summary>
        String PrefNick { get; }

        /// <summary>
        /// Get prefered alternate nick
        /// </summary>
        String PrefNick2 { get; }

        /// <summary>
        /// Get usernick
        /// </summary>
        String UserNick { get; }

        /// <summary>
        /// Get realname
        /// </summary>
        String RealName { get; }

        /// <summary>
        /// Get supported userprefix characters on this IServer
        /// </summary>
        String UserPrefix { get; }

        /// <summary>
        /// Get supported userprefix letters on this IServer
        /// </summary>
        String UserPrefixChars { get; }

        /// <summary>
        /// Get supported channelprefixes on this IServer
        /// </summary>
        String ChannelPrefix { get; }

        /// <summary>
        /// Get server lag
        /// </summary>
        TimeSpan Lag { get; }

        /// <summary>
        /// Returns the IEditbox for the IServer window (can be null)
        /// </summary>
        IEditbox Editbox { get; }

        /// <summary>
        /// Returns the custom default BanType on this IServer, if any
        /// </summary>
        String BanType { get; }

        /// <summary>
        /// Returns the connection id on this IServer
        /// </summary>
        String ConnectionId { get; }

        /// <summary>
        /// Returns number of modes which can be in one MODE request on this IServer
        /// </summary>
        int ModesLength { get; }

        /// <summary>
        /// Returns the available channel modes on this IServer (retrieved from 005 chanmodes=)
        /// </summary>
        string ChannelModes { get; }

        /// <summary>
        /// Returns the available channel modes which can have an argument on this IServer (retrieved from 005 chanmodes=)
        /// </summary>
        ICollection ChannelModesWithArgs { get; }

        /// <summary>
        /// Returns the available channel modes which must have an argument on this IServer (retrieved from 005 chanmodes=)
        /// </summary>
        ICollection ChannelModesWithRequiredArgs { get; }

        /// <summary>
        /// Max topic length on this IServer (retrieved from 005)
        /// </summary>
        int TopicLength { get; }

        /// <summary>
        /// Max ban list count on this IServer (retrieved from 005)
        /// </summary>
        int BanLength { get; }

        /// <summary>
        /// Max except list count on this IServer (retrieved from 005)
        /// </summary>
        int ExceptLength { get; }

        /// <summary>
        /// Max invite list count on this IServer (retrieved from 005)
        /// </summary>
        int InviteLength { get; }

        /// <summary>
        /// Max quiet list count on this IServer (retrieved from 005)
        /// </summary>
        int QuietLength { get; }

        /// <summary>
        /// Returns whether ia away on this IServer
        /// </summary>
        Boolean Away { get; }

        /// <summary>
        /// Returns the away message on this IServer
        /// </summary>
        String AwayMessage { get; }

        /// <summary>
        /// Returns the time user went away on this IServer (UTC time)
        /// </summary>
        DateTime AwayTime { get; }

        /// <summary>
        /// Returns the time user connected to this IServer (UTC time)
        /// </summary>
        DateTime ConnectedAt { get; }

        /// <summary>
        /// Returns the time user last sent a message to this IServer (UTC time)
        /// </summary>
        DateTime IdleTime { get; }

        /// <summary>
        /// Returns whether the last connect attempt was a success
        /// </summary>
        Boolean ConnectionSuccess { get; }

        /// <summary>
        /// Returns whether the IServer is connected
        /// </summary>
        Boolean IsConnected { get; }

        /// <summary>
        /// Returns whether the IServer is connecting
        /// </summary>
        Boolean IsConnecting { get; }

        /// <summary>
        /// Returns whether the IServer is re-connecting
        /// </summary>
        Boolean IsReconnecting { get; }

        /// <summary>
        /// Returns whether the user is logging on IServer
        /// </summary>
        Boolean IsLoggingOn { get; }

        /// <summary>
        /// Returns whether the user is logged on to the IServer
        /// </summary>
        Boolean IsRegistered { get; }

        /// <summary>
        /// Returns whether the last connection attempt failed on the IServer
        /// </summary>
        Boolean ConnectError { get; }

        /// <summary>
        /// Returns whether the error type of the last connections error (0 = no error)
        /// </summary>
        int Error { get; }

        /// <summary>
        /// Returns whether the the IServer uses the global or custom quit message
        /// </summary>
        Boolean UseGlobalQuitMessage { get; }

        /// <summary>
        /// Returns custom QuitMessage on this IServer, if enabled
        /// </summary>
        string QuitMessage { get; }

        /// <summary>
        /// Returns the last QuitMessage sent on this IServer
        /// </summary>
        string LastQuitMessage { get; }

        /// <summary>
        /// Returns the loginmethod for this IServer
        /// </summary>
        int LoginMethod { get; }

        /// <summary>
        /// Returns whether to use network label on this IServer
        /// </summary>
        bool UseNetworkLabel { get; }

        /// <summary>
        /// Network label for this IServer
        /// </summary>
        String NetworkLabel { get; }

        /// <summary>
        /// Returns number of seconds to delay joining channels on this IServer
        /// </summary>
        int DelayJoin { get; }

        /// <summary>
        /// Returns number of seconds to delay between each channel join on this IServer
        /// </summary>
        int DelayBetweenJoins { get; }

        /// <summary>
        /// Returs or set whether to use RegainNick on this IServer
        /// </summary>
        bool RegainNick { get; set; }

        /// <summary>
        /// Sends raw data to the IServer
        /// </summary>
        /// <param name="Data">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendRaw(String Data);

        /// <summary>
        /// Sends raw data to the client IServer, use this to fake messages
        /// E.g when you want to replace something in a PRIVMSG
        /// </summary>
        /// <param name="Data">String</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendFakeRaw(String Data);

        /// <summary>
        /// Finds the matching IChannel.
        /// </summary>
        /// <param name="Name">String</param>
        /// <returns>A IChannel object or null</returns>
        IChannel FindChannel(String Name);

        /// <summary>
        /// Finds the matching User.
        /// </summary>
        /// <param name="Nick">String</param>
        /// <returns>Aa IUser object, if no user is found, a fake IUser object is created (but not stored).</returns>
        IUser FindUser(String Nick);

        /// <summary>
        /// Get a collection of IChannels in this IServer
        /// </summary>
        ICollection GetChannels
        {
            get;
        }

        /// <summary>
        /// Get a collection of IUsers in this IServer
        /// </summary>
        ICollection GetUsers
        {
            get;
        }

        /// <summary>
        /// Sends raw bytes to the IServer, bypasssing any encodings, AdiIRC is not aware when these messages are sent.
        /// </summary>
        /// <param name="Data">Byte[]</param>
        /// <returns>True/False depending if the send was successful</returns>
        Boolean SendRawData(Byte[] Data);

        /// <summary>
        /// Returns Server Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        String Uptime(int type);

        /// <summary>
        /// Connect this IServer if it's not connected
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnect this IServer if it's connected
        /// </summary>
        void Disconnect();
    }

    /// <summary>
    /// Represents a channel object
    /// </summary>
    public interface IChannel
    {
        /// <summary>
        /// Get the IChannel name
        /// </summary>
        String Name { get; }

        /// <summary>
        /// returns the IWindow associated with this IChannel
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get the IServer associated with this IChannel
        /// </summary>
        IServer Server { get; }

        /// <summary>
        /// Get the IChannel topic
        /// </summary>
        String Topic { get; }

        /// <summary>
        /// Get nick of whom set the current topic
        /// </summary>
        String TopicSetBy { get; }

        /// <summary>
        /// Get date of when the current topic was set
        /// </summary>
        String TopicSetAt { get; }

        /// <summary>
        /// Gets whether the IChannel is moderated
        /// </summary>
        Boolean IsModerated { get; }

        /// <summary>
        /// Gets whether the IChannel is invite only
        /// </summary>
        Boolean IsInviteOnly { get; }

        /// <summary>
        /// Gets whether the IChannel is private
        /// </summary>
        Boolean IsPrivate { get; }

        /// <summary>
        /// Gets whether the IChannel is secret
        /// </summary>
        Boolean IsSecret { get; }

        /// <summary>
        /// Gets whether only operators can set topic in IChannel
        /// </summary>
        Boolean OnlyOpsSetTopic { get; }

        /// <summary>
        /// Gets whether the IChannel can recive external messages
        /// </summary>
        Boolean NoExternalMsgs { get; }

        /// <summary>
        /// Gets the IChannel channellimit
        /// </summary>
        int ChannelLimit { get; }

        /// <summary>
        /// Gets the IChannel password
        /// </summary>
        String ChannelPassword { get; }

        /// <summary>
        /// Gets the IChannel mode
        /// </summary>
        String ChannelModes { get; }

        /// <summary>
        /// Gets a collection of users in the IChannel
        /// </summary>
        ICollection GetUsers
        {
            get;
        }

        /// <summary>
        /// Returns the IEditbox for the IChannel window (can be null)
        /// </summary>
        IEditbox Editbox { get; }

        /// <summary>
        /// Returns the IChannel ban list
        /// </summary>
        Collection<IChannelMask> BanList { get; }

        /// <summary>
        /// Returns the IChannel except list
        /// </summary>
        Collection<IChannelMask> ExceptList { get; }

        /// <summary>
        /// Returns the IChannel invite list
        /// </summary>
        Collection<IChannelMask> InviteList { get; }

        /// <summary>
        /// Returns the IChannel quiet list
        /// </summary>
        Collection<IChannelMask> QuietList { get; }

        /// <summary>
        /// Returns the IChannel channel modes with arguments
        /// </summary>
        SortedDictionary<string, string> ChannelModeArgs { get; }
    }

    /// <summary>
    /// Represents a user
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Get user nick
        /// </summary>
        String Nick { get; }

        /// <summary>
        /// returns the IWindow associated with this IUser
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get user ident
        /// </summary>
        String Ident { get; }

        /// <summary>
        /// Get user hostname
        /// </summary>
        String Host { get; }

        /// <summary>
        /// Get user realname
        /// </summary>
        String RealName { get; }

        /// <summary>
        /// Get whether user is IChannel operator (@)
        /// </summary>
        Boolean IsOperator { get; }

        /// <summary>
        /// Get whether user is IChannel special operator (!)
        /// </summary>
        Boolean IsPOperator { get; }

        /// <summary>
        /// Get whether user is IChannel owner (&amp;)
        /// </summary>
        Boolean IsOwner { get; }

        /// <summary>
        /// Get whether user has IChannel voice (+)
        /// </summary>
        Boolean HasVoice { get; }

        /// <summary>
        /// Get whether user is IChannel half operator (%)
        /// </summary>
        Boolean IsHalfOperator { get; }

        /// <summary>
        /// Get whether user is away
        /// </summary>
        Boolean IsAway { get; }

        /// <summary>
        /// /// Returns the time user last sent a message to this IChannel (UTC time)
        /// </summary>
        DateTime IdleTime { get; }

        /// <summary>
        /// /// Returns the time the user joined the IChannel (UTC time)
        /// </summary>
        DateTime Joined { get; }

        /// <summary>
        /// /// Returns whether the user has spoken since joining the IChannel
        /// </summary>
        Boolean HasSpoken { get; }

        /// <summary>
        /// /// Returns the nick color to use for the nick on this IChannel (UTC time)
        /// </summary>
        int NickColor { get; }

        /// <summary>
        /// /// Returns the rgb nick color to use for the nick on this IChannel (UTC time)
        /// </summary>
        string RgbNickColor { get; }

        /// <summary>
        /// Retrive all channels this IUser is in
        /// </summary>
        ICollection GetChannels { get; }

        /// <summary>
        /// Gets or sets the nickcolor,
        /// you can use any color from 1 to 80 from the client colors,
        /// this will override any random nick color
        /// </summary>
        //int NickColor { get; set; }

        /// <summary>
        /// Returns the IEditbox for the IUser/private window (can be null)
        /// </summary>
        IEditbox Editbox { get; }
    }

    /// <summary>
    /// Represents a form window
    /// </summary>
    public interface IWindow
    {
        /// <summary>
        /// Return the name of the IWindow
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Return the window name of the IWindow
        /// </summary>
        String NiceName { get; }

        /// <summary>
        /// Returns the IWindow title text
        /// </summary>
        String Text { get; }

        /// <summary>Return the IWindow window handle</summary>
        IntPtr WindowHandle { get; }

        /// <summary>
        /// Returns whether the IWindow is a MDI child
        /// </summary>
        Boolean IsMdiChild { get; }

        /// <summary>
        /// Returns the IWindow window state
        /// </summary>
        FormWindowState WindowState { get; set; }

        /// <summary>
        /// Returns whether the IWindow is visible
        /// </summary>
        Boolean Visible { get; set; }

        /// <summary>
        /// Returns the name of the log file associated with this IWindow
        /// </summary>
        String LogFile { get; }

        /// <summary>
        /// Returns the WindowType of this IWindow
        /// </summary>
        WindowType WindowType { get; }

        /// <summary>
        /// Returns the WindowType of this IWindow in a numeric form
        /// </summary>
        int WindowTypeNum { get; }

        /// <summary>
        /// Returns the window id of this IWindow
        /// </summary>
        int WindowId { get; }

        /// <summary>
        /// Returns the IEditbox associated with this IWindow
        /// </summary>
        IEditbox Editbox { get; }

        /// <summary>
        /// Returns the ITextView associated with this IWindow
        /// </summary>
        ITextView TextView { get; }

        /// <summary>
        /// Returns number of unread messages in this IWindow
        /// </summary>
        int UnreadMessages { get; }

        /// <summary>
        /// Returns the IServer associated with this IWindow
        /// </summary>
        IServer Server { get; }

        /// <summary>
        /// Returns the IChannel associated with this IWindow
        /// </summary>
        IChannel Channel { get; }

        /// <summary>
        /// Returns the IUser associated with this IWindow
        /// </summary>
        IUser User { get; }

        /// <summary>
        /// Returns the ICustomWindow associated with this IWindow
        /// </summary>
        ICustomWindow CustomWindow { get; }
    }

    /// <summary>
    /// Represents a custom window
    /// </summary>
    public interface ICustomWindow
    {
        /// <summary>
        /// Retrive the custom window name
        /// </summary>
        String Name { get; }

        /// <summary>
        /// returns the IWindow associated with this ICustomWindow
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Returns True if the custom window is a picture window, otherwise false
        /// </summary>
        Boolean PictureWindow { get; }

        /// <summary>
        /// Returns the IEditbox for the ICustomwindow (can be null)
        /// </summary>
        IEditbox Editbox { get; }
    }

    /// <summary>
    /// Represents a Editbox
    /// </summary>
    public interface IEditbox
    {
        /// <summary>
        /// Returns the IEditbox text
        /// </summary>
        String Text { get; set; }

        /// <summary>
        /// Returns the selection start position in the IEditbox
        /// </summary>
        int SelectionStart { get; set; }

        /// <summary>
        /// Returns the selection length in the IEditbox
        /// </summary>
        int SelectionLength { get; set; }

        /// <summary>
        /// Returns the selected text in the IEditbox
        /// </summary>
        String SelectedText { get; set; }

        /// <summary>
        /// Selects a substring of the Text
        /// </summary>
        /// <param name="Start">The start position to select</param>
        /// <param name="Length">The length to select</param>
        void Select(int Start, int Length);
    }

    /// <summary>
    /// Represents a TextView
    /// </summary>
    public interface ITextView
    {
        /// <summary>
        /// Returns whether time stamps is enabled for this ITextView buffer
        /// </summary>
        Boolean UseTimestamp { get; }

        /// <summary>
        /// Returns the maxmimum buffer for this ITextView
        /// </summary>
        int MaxBuffer { get; set; }

        /// <summary>
        /// Returns the maxmimum number the scrollbar can scroll to (wrapped lines)
        /// </summary>
        int MaxScrollbar { get; }

        /// <summary>
        /// Returns the current scrollbar position (wrapped lines)
        /// </summary>
        int ScrollbarPos { get; }

        /// <summary>
        /// Returns the !TextView buffer lines
        /// </summary>
        ICollection Lines { get; }

        /// <summary>
        /// Returns the Nth ITextView buffer line
        /// </summary>
        string GetLine(int line);

        /// <summary>
        /// Adds a line of text to the ITextView buffer
        /// Adding lines manually requires all formatting to be done manually as well, format = 
        /// timestamp + " " + ITools.NickColumChar + nick + ITools.NickColumChar + " " + text
        /// </summary>
        void Add(String Text, int lineColor, string rgbLineColor);

        /// <summary>
        /// Inserts a line of text at the Nth line in the ITextView buffer
        /// Adding lines manually requires all formatting to be done manually as well, format = 
        /// timestamp + " " + ITools.NickColumChar + nick + ITools.NickColumChar + " " + text
        /// </summary>
        void Insert(String text, int line, int lineColor, string rgbLineColor);

        /// <summary>
        /// Replaces the Nth line in the ITextView buffer with a new text
        /// Adding lines manually requires all formatting to be done manually as well, format = 
        /// timestamp + " " + ITools.NickColumChar + nick + ITools.NickColumChar + " " + text
        /// </summary>
        void Replace(String text, int line, int lineColor, string rgbLineColor);

        /// <summary>
        /// Removes the Nth ITextView buffer line
        /// </summary>
        void Remove(int start, int end);

        /// <summary>
        /// Clears the ITextView buffer
        /// </summary>
        void Clear();

        /// <summary>
        /// Scrolls the scrollbar to the Nth (wrapped) line
        /// </summary>
        void ScrollTo(int line);

        /// <summary>
        /// Scrolls the scrollbar to the unread line marker
        /// </summary>
        void ScrollToUnreadLine();

        /// <summary>
        /// Scrolls the scrollbar to the start
        /// </summary>
        void ScrollHome();

        /// <summary>
        /// Scrolls the scrollbar to the end
        /// </summary>
        void ScrollEnd();

        /// <summary>
        /// Scrolls the scrollbar down 1 (wrapped line)
        /// </summary>
        void ScrollDown();

        /// <summary>
        /// Scrolls the scrollbar up 1 (wrapped line)
        /// </summary>
        void ScrollUp();

        /// <summary>
        /// Scrolls the scrollbar 1 page up
        /// </summary>
        void ScrollPageUp();

        /// <summary>
        /// Scrolls the scrollbar 1 page down
        /// </summary>
        void ScrollPageDown();
    }

    /// <summary>
    /// Various tools
    /// </summary>
    public interface ITools
    {
        /// <summary>
        /// Gets the bold character
        /// </summary>
        Char BoldChar { get; }

        /// <summary>
        /// Gets the color character
        /// </summary>
        Char ColorChar { get; }

        /// <summary>
        /// Gets the underline character
        /// </summary>
        Char UnderLineChar { get; }

        /// <summary>
        /// Gets the action character
        /// </summary>
        Char ActionChar { get; }

        /// <summary>
        /// Gets the nick column character used to determine where in the ITextView buffer the nick coulmn should be
        /// </summary>
        Char NickColumnChar { get; }

        /// <summary>
        /// Gets the link character used to determine if the text in the ITextView buffer is a link
        /// </summary>
        Char LinkChar { get; }

        /// <summary>
        /// Gets path sperator / or \ , for future use
        /// </summary>
        String PS { get; }

        /// <summary>
        /// Returns a string with colors stripped out
        /// </summary>
        /// <param name="Str">String</param>
        /// <returns>A stripped string</returns>
        String StripColors(String Str);

        /// <summary>
        /// Returns a string with bold stripped out
        /// </summary>
        /// <param name="Str">String</param>
        /// <returns>A stripped string</returns>
        String StripBold(String Str);

        /// <summary>
        /// Returns a string with italic stripped out
        /// </summary>
        /// <param name="Str">String</param>
        /// <returns>A stripped string</returns>
        String StripItalic(String Str);

        /// <summary>
        /// Returns a string with underline stripped out
        /// </summary>
        /// <param name="Str">String</param>
        /// <returns>A stripped string</returns>
        String StripUnderline(String Str);

        /// <summary>
        /// Use this to write to debug.txt in the main folder, returns true if the debug was written
        /// </summary>
        /// <param name="Str">String</param>
        /// <returns>True/False depending if the write was successful</returns>
        Boolean Debug(String Str);

        /// <summary>
        /// Converts string to DateTime, see Time variables for possible syntax
        /// </summary>
        /// <param name="Syntax">String</param>
        /// <param name="Time">DateTime</param>
        /// <returns>A parsed string</returns>
        String ParseTime(String Syntax, DateTime Time);

        /// <summary>
        /// Converts unix timestamp to a DateTime
        /// </summary>
        /// <param name="Time">Double</param>
        /// <returns>A Datetime value</returns>
        DateTime UnixTimeToDate(Double Time);

        /// <summary>
        /// Converts DateTime to unix timestamp
        /// </summary>
        /// <param name="Time">DateTime</param>
        /// <returns>A unix timestamp value</returns>
        long DateToUnixTime(DateTime Time);

        /// <summary>
        /// Checks if a string is numeric
        /// </summary>
        /// <param name="Str">String</param>
        /// <returns>True/False whether the string is a number</returns>
        Boolean IsNumeric(String Str);

        /// <summary>
        /// Open ups a tool window
        /// </summary>
        /// <param name="Height">int</param>
        /// <param name="Width">int</param>
        /// <param name="Text">String</param>
        /// <param name="Title">String</param>
        void ToolWindow(int Height, int Width, String Title, String Text);

        /// <summary>
        /// Makes the text bold if Boldify is enabled
        /// </summary>
        /// <param name="text">Text to boldify</param>
        /// <returns>Boldified text</returns>
        String Boldify(String text);

        /// <summary>
        /// Splits a string by a string
        /// </summary>
        /// <param name="Str">String</param>
        /// <param name="SplitBy">String</param>
        /// <returns>A list of strings</returns>
        String[] SplitString(String Str, String SplitBy);

        /// <summary>
        /// Returns system information based on Syntax, see system variables for more information
        /// </summary>
        /// <param name="Str">String</param>
        /// <param name="Args">String</param>
        /// <returns>A oarsed string</returns>
        String ParseSysinfo(String Str, String Args);
    }

    /// <summary>
    /// Interface for returning Messages options
    /// </summary>
    public interface IMessagesOptions
    {
        /// <summary>
        /// Returns the nick prefix/syntax to use for regular messages
        /// </summary>
        String PrefixUser { get; set; }

        /// <summary>
        /// Returns the nick prefix/syntax to use for CTCP messages
        /// </summary>
        String PrefixCtcp { get; set; }

        /// <summary>
        /// Returns the nick prefix/syntax to use for emote messages
        /// </summary>
        String PrefixEmote { get; set; }

        /// <summary>
        /// Returns the nick prefix/syntax to use for system messages
        /// </summary>
        String PrefixSys { get; set; }

        /// <summary>
        /// Returns the time format to use for messages
        /// </summary>
        String TimeFormat { get; set; }

        /// <summary>
        /// Returns whether global timestamps is enabled or disabled
        /// </summary>
        Boolean UseTimestamp { get; set; }
    }

    /// <summary>
    /// Interface for returning Editbox options
    /// </summary>
    public interface IEditboxOptions
    {
        /// <summary>
        /// Text to add when tabbing nicks
        /// </summary>
        String TabCompleteSuffix { get; set; }

        /// <summary>
        /// Text to add when tabbing nicks and the nick is the first word in the IEditbox
        /// </summary>
        String TabCompleteSuffixFirstWord { get; set; }
    }

    /// <summary>
    /// Interface for ban/except/invite/quiet lists
    /// </summary>
    public interface IChannelMask
    {
        string Mask { get; }
        long SetAt { get; }
        String SetBy { get; }
    }

    /// <summary>
    /// RawDataArgs
    /// </summary>
    public class RawDataArgs : EventArgs
    {
        /// <summary>
        /// RawDataArgs
        /// </summary>
        /// <param name="server"></param>
        /// <param name="bytes"></param>
        public RawDataArgs(IServer server, byte[] bytes)
        {
            Server = server;
            Bytes = bytes;
        }

        /// <summary>
        /// The IServer of this event.
        /// </summary>
        public IServer Server { get; set; }

        /// <summary>
        /// The raw bytes of this event.
        /// </summary>
        public byte[] Bytes { get; set; }
    }
}
