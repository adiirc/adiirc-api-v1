﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Interfaces;

    public class NickArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IUser user;
        private readonly string newNick;

        public NickArgs(IWindow window, IServer server, IUser user, string newNick)
        {
            this.window = window;
            this.server = server;
            this.user = user;
            this.newNick = newNick;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IUser User { get { return this.user; } }

        public string NewNick { get { return this.newNick; } }
    }
}
