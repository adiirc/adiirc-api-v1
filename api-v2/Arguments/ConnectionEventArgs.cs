﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class ConnectionEventArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private EatData eatData;

        public ConnectionEventArgs(IWindow window, IServer server, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
