﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class KickArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser kickedUser;
        private readonly IUser byUser;
        private readonly string kickReason;
        private EatData eatData;

        public KickArgs(IWindow window, IServer server, IChannel channel, IUser kickedUser, IUser byUser, string kickReason, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.kickedUser = kickedUser;
            this.byUser = byUser;
            this.kickReason = kickReason;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser KickedUser { get { return this.kickedUser; } }

        public IUser ByUser { get { return this.byUser; } }

        public string KickReason { get { return this.kickReason; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
