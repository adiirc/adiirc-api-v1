﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class CommandArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly string command;
        private EatData eatData;

        public CommandArgs(IWindow window, string command, EatData eatData)
        {
            this.window = window;
            this.command = command;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public string Command { get { return this.command; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
