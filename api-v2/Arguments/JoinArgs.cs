﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class JoinArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IChannel channel;
        private readonly IUser user;
        private EatData eatData;

        public JoinArgs(IWindow window, IChannel channel, IUser user, EatData eatData)
        {
            this.window = window;
            this.channel = channel;
            this.user = user;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
