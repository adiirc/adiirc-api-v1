﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class TopicArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly string newTopic;
        private EatData eatData;

        public TopicArgs(IWindow window, IServer server, IChannel channel, IUser user, string newTopic, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.newTopic = newTopic;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public string NewTopic { get { return this.newTopic; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
