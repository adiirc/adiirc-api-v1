﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class UserModeArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly String mode;
        private EatData eatData;

        public UserModeArgs(IWindow window, IServer server, String mode, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.mode = mode;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public String Mode { get { return this.mode; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
