﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class QuitArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IUser user;
        private readonly String quitMessage;
        private EatData eatData;

        public QuitArgs(IWindow window, IServer server, IUser user, String quitMessage, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.user = user;
            this.quitMessage = quitMessage;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IUser User { get { return this.user; } }

        public String QuitMessage { get { return this.quitMessage; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
