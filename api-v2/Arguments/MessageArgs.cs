﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class MessageArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly String message;
        private EatData eatData;

        public MessageArgs(IWindow window, IServer server, IChannel channel, IUser user, String message, EatData eatData)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.message = message;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public String Message { get { return this.message; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
