﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using Enumerators;
    using Interfaces;

    public class PartArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly string partMessage;
        private EatData eatData;

        public PartArgs(IWindow window, IChannel channel, IUser user, string partMessage, EatData eatData)
        {
            this.window = window;
            this.channel = channel;
            this.user = user;
            this.partMessage = partMessage;
            this.eatData = eatData;
        }

        public IWindow Window { get { return this.window; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public string PartMessage { get { return this.partMessage; } }

        public EatData EatData { get { return this.eatData; } set { this.eatData = value; } }
    }
}
