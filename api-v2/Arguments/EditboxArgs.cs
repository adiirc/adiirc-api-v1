﻿namespace AdiIRCAPIv2.Arguments
{
    using System;
    using System.Windows.Forms;
    using Interfaces;

    public class EditboxArgs : EventArgs
    {
        private readonly IWindow window;
        private readonly IServer server;
        private readonly IChannel channel;
        private readonly IUser user;
        private readonly IEditbox editbox;
        private readonly KeyEventArgs keyEventArgs;

        public EditboxArgs(IWindow window, IServer server, IChannel channel, IUser user, IEditbox editbox, KeyEventArgs keyEventArgs)
        {
            this.window = window;
            this.server = server;
            this.channel = channel;
            this.user = user;
            this.editbox = editbox;
            this.keyEventArgs = keyEventArgs;
        }

        public IWindow Window { get { return this.window; } }

        public IServer Server { get { return this.server; } }

        public IChannel Channel { get { return this.channel; } }

        public IUser User { get { return this.user; } }

        public IEditbox Editbox { get { return this.editbox; } }

        public KeyEventArgs KeyEventArgs { get { return this.keyEventArgs; } }
    }
}
