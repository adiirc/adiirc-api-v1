﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = false)]
[assembly: ComVisible(true)]
[assembly: Guid("16d56dc8-25f6-42aa-b831-4eeac242dfbf")]
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]

namespace AdiIRCAPIv2
{
    using Arguments;

    /// <summary>
    /// Delegate gets called when the user changes a options and the config file is reloaded.
    /// </summary>
    public delegate void OptionsChanged();

    /// <summary>
    /// Delegate gets called when a key is pressed down in a Editbox
    /// </summary>
    /// <param name="e">EditboxArgs</param>
    public delegate void EditboxKeyDown(EditboxArgs e);

    /// <summary>
    /// Delegate gets called when a key is released in a IEditbox
    /// </summary>
    /// <param name="e">EditboxArgs</param>
    public delegate void EditboxKeyUp(EditboxArgs e);

    /// <summary>
    /// Delegate gets called when a menu is opened
    /// </summary>
    /// <param name="e">MenuEventArgs</param>
    public delegate void Menu(MenuEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer connects
    /// </summary>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void Connect(ConnectionEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer disconnects
    /// </summary>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void Disconnect(ConnectionEventArgs e);

    /// <summary>
    /// Delegate gets called after an IServer successfully logs on to an IRC server
    /// </summary>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void Registered(ConnectionEventArgs e);

    /// <summary>
    /// Delegate gets called when a IServer retrives raw data, equal to RAW *:*:
    /// AdiIRC will have already parsed the data into a string.
    /// </summary>
    /// <param name="e">DataArgs</param>
    public delegate void DataReceived(DataArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives a notice
    /// </summary>
    /// <param name="e">NoticeArgs</param>
    public delegate void PrivateNotice(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel recives a notice
    /// </summary>
    /// <param name="e">NoticeArgs</param>>
    public delegate void ChannelNotice(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IServer recives a notice
    /// </summary>
    /// <param name="e">NoticeArgs</param>
    public delegate void ServerNotice(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel revices a message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void ChannelMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives a private message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void PrivateMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives a private action message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void PrivateActionMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives a channel action message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void ChannelActionMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel receives a CTCP message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void ChannelCtcpMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when your IUser receives a CTCP message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void PrivateCtcpMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel receives a CTCPReply message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void ChannelCtcpReplyMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when your IUser receives a CTCP Reply message
    /// </summary>
    /// <param name="e">MessageArgs</param>
    public delegate void PrivateCtcpReplyMessage(MessageArgs e);

    /// <summary>
    /// Delegate gets called when you send a message to a IChannel or IUser
    /// </summary>
    /// <param name="e">MessageArgs e</param>
    public delegate void MessageSent(MessageArgs e);

    /// <summary>
    /// Delegate gets called when a IChannel revices a mode
    /// </summary>
    /// <param name="e">ModeArgs</param>
    public delegate void ChannelMode(ModeArgs e);

    /// <summary>
    /// Delegate gets called when your IUser revices a umode
    /// </summary>
    /// <param name="e">UserModeArgs</param>
    public delegate void UserMode(UserModeArgs e);

    /// <summary>
    /// Delegate gets called when a user on IChannel revices an invite
    /// </summary>
    /// <param name="e">InviteArgs</param>
    public delegate void UserInvitedToChannel(InviteArgs e);

    /// <summary>
    /// Delegate gets called when your IUser recives an IChannel invite
    /// </summary>
    /// <param name="e">InviteArgs</param>
    public delegate void ChannelInvite(InviteArgs e);

    /// <summary>
    /// Delegate gets called when a IUser quits the IServer
    /// </summary>
    /// <param name="e">QuitArgs</param>
    public delegate void Quit(QuitArgs e);

    /// <summary>
    /// Delegate gets called when a IUser change nick
    /// </summary>
    /// <param name="e">NickArgs</param>
    public delegate void Nick(NickArgs e);

    /// <summary>
    /// Delegate gets called when a IUser changes topic in IChannel
    /// </summary>
    /// <param name="e">TopicArgs</param>
    public delegate void ChannelTopic(TopicArgs e);

    /// <summary>
    /// Delegate gets called when a IUser is kicked from IChannel
    /// </summary>
    /// <param name="e">KickArgs</param>
    public delegate void ChannelKick(KickArgs e);

    /// <summary>
    /// Delegate gets called when a IUser joines a IChannel
    /// </summary>
    /// <param name="e">JoinArgs</param>
    public delegate void ChannelJoin(JoinArgs e);

    /// <summary>
    /// Delegate gets called when a IUser parts a IChannel
    /// </summary>
    /// <param name="e">PartArgs</param>
    public delegate void ChannelPart(PartArgs e);

    /// <summary>
    /// Delegate gets called when a hooked command is called as a /command
    /// </summary>
    /// <param name="e">Command</param>
    public delegate void Command(CommandArgs e);

    /// <summary>
    /// Delegate gets called when a hooked command is called as a $identifier
    /// </summary>
    /// <param name="e">Command</param>
    public delegate void Identifier(CommandArgs e);

    /// <summary>
    /// Delegate gets called when any raw data is recieved to the IServer.
    /// You can modify the byte array args, AdiIRC will then parse this array with the proper encoding.
    /// If the array is set to null or zero bytes, the raw message is ignored by AdiIRC.
    /// </summary>
    /// <param name="e">RawBytesArgs</param>
    public delegate void RawData(RawBytesArgs e);

    /// <summary>
    /// Delegate gets called when any raw data is recieved to the IServer.
    /// You can modify the byte array args, AdiIRC will then parse this array with the proper encoding.
    /// If the array is set to null or zero bytes, the raw message is ignored by AdiIRC.
    /// </summary>
    /// <param name="e">RawBytesReceivedArgs</param>
    public delegate void RawBytesReceived(RawBytesArgs e);

    /// <summary>
    /// Delegate gets called when any raw data is sent to the IServer.
    /// You can modify the byte array args, AdiIRC will then send this array to the server.
    /// </summary>
    /// <param name="e">RawBytesSendArgs</param>
    public delegate void RawBytesSent(RawBytesArgs e);

    /// <summary>
    /// Delegate gets called when user sends data to the IServer.
    /// AdiIRC will have already parsed the data into a string.
    /// </summary>
    /// <param name="e">DataArgs</param>
    public delegate void DataSent(DataArgs e);

    /* Delegates To Do:
     *   Speaking agent related stuff
     *   DCC related stuff
     *   Dialog creation/manipulation stuff
     *   
     *   ActiveWindowChange      (on ACTIVE)
     *   AppActiveChange         (on APPACTIVE)
     *   ChannelBanSet           (on BAN)
     *   ChannelBanUnset         (on UNBAN)
     *   ChannelUserOped         (on OP)
     *   ChannelUserUnoped       (on DEOP)
     *   ChannelUserHalfOped     (on HELP)
     *   ChannelUserUnhalfOpened (on DEHELP)
     *   ChannelUserVoiced       (on VOICE)
     *   ChannelUserUnvoiced     (on DEVOICE)
     *   WindowOpen              (on OPEN)
     *   WindowClose             (on CLOSE)
     *   ConnectFailed           (on CONNECTFAIL)
     *   DnsResolution           (on DNS)
     *   ServerError             (on ERROR)
     *   AppExit                 (on EXIT)
     *   CursorHoversOnText      (on ^HOTLINK)
     *   CursorClicksText        (on HOTLINK)
     *   ??                      (on INPUT)
     *   
     *   - More to come after this push
     */
}
