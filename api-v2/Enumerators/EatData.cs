﻿namespace AdiIRCAPIv2.Enumerators
{
    /// <summary>
    /// Return data to the delegates stating whether the program should ignore none, ignore text output, or ignore everything
    /// Use with caution
    /// </summary>
    public enum EatData
    {
        /// <summary>
        /// Hide no part of the event.
        /// </summary>
        EatNone = 0,
        /// <summary>
        /// Hide the text output from this event.
        /// </summary>
        EatText = 1,
        /// <summary>
        /// Hide the entire event.
        /// </summary>
        EatAll = 2
    }
}
