﻿namespace AdiIRCAPIv2
{
    using System.Collections;
    using Interfaces;

    /// <summary>
    /// Represents a user
    /// </summary>
    public interface IPrivate : IWindow
    {
        /// <summary>
        /// Get user nick
        /// </summary>
        string Nick { get; }

        /// <summary>
        /// Get user ident
        /// </summary>
        string Ident { get; }

        /// <summary>
        /// Get user hostname
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Get user realname
        /// </summary>
        string RealName { get; }

        bool IsAway { get; }

        /// <summary>
        /// Retrive all channels this IUser is in
        /// </summary>
        ICollection GetChannels { get; }
    }
}
