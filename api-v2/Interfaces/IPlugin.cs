﻿namespace AdiIRCAPIv2
{
    /// <summary>
    /// Inherit from this class in your plugin, e.g class MyPlugin : IPlugin
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Returns current plugin manager
        /// </summary>
        IPluginHost Host { get; set; }

        /// <summary>
        /// Returns ITools reference
        /// </summary>
        ITools Tools { get; set; }

        /// <summary>
        /// Get/Set plugin name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Get/Set plugin description
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Get/Set your name/nick
        /// </summary>
        string Author { get; }

        /// <summary>
        /// Get/Set plugin version
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Your email for support, optional, set to "" if you dont want to provide a email.
        /// </summary>
        string Email { get; }

        /// <summary>
        /// Called when plugin is loaded
        /// </summary>
        void Initialize();

        /// <summary>
        /// Called when plugin is unloaded
        /// </summary>
        void Dispose();
    }


}
