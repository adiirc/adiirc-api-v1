﻿namespace AdiIRCAPIv2
{
    using System;
    using System.Collections;
    using Interfaces;

    /// <summary>
    /// Represents a user
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Get user nick
        /// </summary>
        string Nick { get; }

        /// <summary>
        /// returns the IWindow associated with this IUser
        /// </summary>
        IWindow Window { get; }

        /// <summary>
        /// Get user ident
        /// </summary>
        string Ident { get; }

        /// <summary>
        /// Get user hostname
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Get user realname
        /// </summary>
        string RealName { get; }

        /// <summary>
        /// Get whether user is IChannel operator (@)
        /// </summary>
        bool IsOperator { get; }

        /// <summary>
        /// Get whether user is IChannel special operator (!)
        /// </summary>
        bool IsPOperator { get; }

        /// <summary>
        /// Get whether user is IChannel owner (&amp;)
        /// </summary>
        bool IsOwner { get; }

        /// <summary>
        /// Get whether user has IChannel voice (+)
        /// </summary>
        bool HasVoice { get; }

        /// <summary>
        /// Get whether user is IChannel half operator (%)
        /// </summary>
        bool IsHalfOperator { get; }

        /// <summary>
        /// Get whether user is away
        /// </summary>
        bool IsAway { get; }

        /// <summary>
        /// /// Returns the time user last sent a message to this IChannel (UTC time)
        /// </summary>
        DateTime IdleTime { get; }

        /// <summary>
        /// /// Returns the time the user joined the IChannel (UTC time)
        /// </summary>
        DateTime Joined { get; }

        /// <summary>
        /// /// Returns whether the user has spoken since joining the IChannel
        /// </summary>
        bool HasSpoken { get; }

        /// <summary>
        /// /// Returns the nick color to use for the nick on this IChannel (UTC time)
        /// </summary>
        int NickColor { get; }

        /// <summary>
        /// /// Returns the rgb nick color to use for the nick on this IChannel (UTC time)
        /// </summary>
        string RgbNickColor { get; }

        /// <summary>
        /// Retrive all channels this IUser is in
        /// </summary>
        ICollection GetChannels { get; }

        /// <summary>
        /// Gets or sets the nickcolor,
        /// you can use any color from 1 to 80 from the client colors,
        /// this will override any random nick color
        /// </summary>
        //int NickColor { get; set; }

        /// <summary>
        /// Returns the IEditbox for the IUser/private window (can be null)
        /// </summary>
        IEditbox Editbox { get; }
    }
}
