﻿namespace AdiIRCAPIv2.Interfaces
{
    /// <summary>
    /// Interface for ban/except/invite/quiet lists
    /// </summary>
    public interface IChannelMask
    {
        string Mask { get; }
        long SetAt { get; }
        string SetBy { get; }
    }
}
