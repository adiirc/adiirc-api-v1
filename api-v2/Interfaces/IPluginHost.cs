﻿namespace AdiIRCAPIv2
{
    using System;
    using System.Collections;
    using Interfaces;

    /// <summary>
    /// Overall plugin manager
    /// </summary>
    public interface IPluginHost
    {
        /// <summary>
        /// Delegate gets called when the user changes a options and the config file is reloaded.
        /// </summary>
        event OptionsChanged OnOptionsChanged;

        /// <summary>
        /// Delegate gets called when a key is pressed down in a IEditbox
        /// </summary>
        event EditboxKeyDown OnEditboxKeyDown;

        /// <summary>
        /// Delegate gets called when a key is pressed up in a IEditbox
        /// </summary>
        event EditboxKeyUp OnEditboxKeyUp;

        /// <summary>
        /// Delegate gets called when a menu is opened
        /// </summary>
        event Menu OnMenu;

        /// <summary>
        /// Delegate gets called when a IServer connects
        /// </summary>
        event Connect OnConnect;

        /// <summary>
        /// Delegate gets called when a IServer disconnects
        /// </summary>
        event Disconnect OnDisconnect;

        /// <summary>
        /// Delegate gets called when a IServer is registered
        /// </summary>
        event Registered OnRegistered;

        /// <summary>
        /// Delegate gets called when a IServer retrives raw data
        /// </summary>
        event DataReceived OnDataReceived;

        /// <summary>
        /// Delegate gets called when you receive a notice message
        /// </summary>
        event PrivateNotice OnPrivateNotice;

        /// <summary>
        /// Delegate gets called when a IChannel receives a notice message
        /// </summary>
        event ChannelNotice OnChannelNotice;

        /// <summary>
        /// Delegate gets called when a IServer receives a notice message
        /// </summary>
        event ServerNotice OnServerNotice;

        /// <summary>
        /// Delegate gets called when a IChannel revices a message
        /// </summary>
        event ChannelMessage OnChannelMessage;

        /// <summary>
        /// Delegate gets called when your IUser recives a private action message
        /// </summary>
        event PrivateMessage OnPrivateMessage;

        /// <summary>
        /// Delegate gets called when your IUser receives a private message
        /// </summary>
        event PrivateActionMessage OnPrivateActionMessage;

        /// <summary>
        /// Delegate gets called when your IUser recives a channel action message
        /// </summary>
        event ChannelActionMessage OnChannelActionMessage;

        /// <summary>
        /// Delegate gets called when a IChannel receives a CTCP message
        /// </summary>
        /// <param name="e">MessageArgs</param>
        event ChannelCtcpMessage OnChannelCtcpMessage;

        /// <summary>
        /// Delegate gets called when your IUser receives a CTCP message
        /// </summary>
        event PrivateCtcpMessage OnPrivateCtcpMessage;

        /// <summary>
        /// Delegate gets called when a IChannel receives a CTCPReply message
        /// </summary>
        event ChannelCtcpReplyMessage OnChannelCtcpReplyMessage;

        /// <summary>
        /// Delegate gets called when your IUser receives a CTCP Reply message
        /// </summary>
        event PrivateCtcpReplyMessage OnPrivateCtcpReplyMessage;

        /// <summary>
        /// Delegate gets called when your IUser sends a message to a IChannel or IUser
        /// </summary>
        event MessageSent OnMessageSent;

        /// <summary>
        /// Delegate gets called when a IChannel receives a mode
        /// </summary>
        event ChannelMode OnChannelMode;

        /// <summary>
        /// Delegate gets called when your IUser receives a umode
        /// </summary>
        event UserMode OnUserMode;

        /// <summary>
        /// Delegate gets called when a user on IChannel receives an invite
        /// </summary>
        event UserInvitedToChannel OnUserInvitedToChannel;

        /// <summary>
        /// Delegate gets called when your IUser receives an IChannel invite
        /// </summary>
        event ChannelInvite OnChannelInvite;

        /// <summary>
        /// Delegate gets called when a IUser quits the IServer
        /// </summary>
        event Quit OnQuit;

        /// <summary>
        /// Delegate gets called when a IUser change nick
        /// </summary>
        event Nick OnNick;

        /// <summary>
        /// Delegate gets called when a IUser changes topic in IChannel
        /// </summary>
        event ChannelTopic OnChannelTopic;

        /// <summary>
        /// Delegate gets called when a IUser is kicked from IChannel
        /// </summary>
        event ChannelKick OnChannelKick;

        /// <summary>
        /// Delegate gets called when a IUser parts a IChannel
        /// </summary>
        event ChannelPart OnChannelPart;

        /// <summary>
        /// Delegate gets called when a IUser joines a IChannel
        /// </summary>
        event ChannelJoin OnChannelJoin;

        /// <summary>
        /// Delegate gets called when your user types a command
        /// </summary>
        event Command OnCommand;

        /// <summary>
        /// Delegate gets called when the command is called as a $identifier in a script
        /// </summary>
        event Identifier OnIdentifier;

        /// <summary>
        /// Sends raw bytes to the IServer, bypasssing any encodings, AdiIRC is not aware when this messages are sent.
        /// </summary>
        event RawData OnRawData;

        /// <summary>
        /// Delegate gets called when any raw data is recieved to the IServer.
        /// You can modify the byte array args, AdiIRC will then parse this array with the proper encoding.
        /// If the array is set to null or zero bytes, the raw message is ignored by AdiIRC.
        /// </summary>
        event RawBytesReceived OnRaw;

        /// <summary>
        /// Delegate gets called when any raw data is sent to the IServer.
        /// You can modify the byte array args, AdiIRC will then send this array to the server.
        /// </summary>
        event RawBytesSent OnRawBytesSent;

        /// <summary>
        /// Delegate gets called when user sends data to the IServer.
        /// AdiIRC will have already parsed the data into a string.
        /// </summary>
        event DataSent OnDataSent;

        /// <summary>
        /// Returns the main window handle
        /// </summary>
        IntPtr MainWindowHandle { get; }

        /// <summary>
        /// Returns the current active IWindow
        /// </summary>
        IWindow ActiveIWindow { get; }

        /// <summary>
        /// IMessagesOptions
        /// </summary>
        IMessagesOptions MessagesOptions { get; }

        /// <summary>
        /// IEditboxOptions
        /// </summary>
        IEditboxOptions EditboxOptions { get; }

        /// <summary>
        /// Returns a list of global variables which are saved across sessions
        /// </summary>
        IDictionary GetVariables { get; }

        /// <summary>
        /// Use this to add /commands, you can then suscribe to OnCommand to get data from this command
        /// </summary>
        /// <param name="plugin">IPlugin</param>
        /// <param name="command">String</param>
        /// <param name="helpSyntax">String</param>
        /// <param name="description">String</param>
        /// <returns>True/False depending if hooking the command was successful</returns>
        bool HookCommand(IPlugin plugin, string command, string helpSyntax, string description);

        /// <summary>
        /// Remove a /command, you have defined.
        /// </summary>
        /// <param name="plugin">IPlugin</param>
        /// <param name="command">String</param>
        /// <returns>True/False depending if unhooking the command was successful</returns>
        bool UnHookCommand(IPlugin plugin, string command);

        /// <summary>
        /// Get a list of IServers
        /// </summary>
        ICollection GetServers
        {
            get;
        }

        /// <summary>
        /// Get a list of IWindows
        /// </summary>
        ICollection GetWindows { get; }

        /// <summary>
        /// Returns the AdiIRC config folder.
        /// </summary>
        string ConfigFolder { get; }

        /// <summary>
        /// Returns the AdiIRC program folder.
        /// </summary>
        string ProgramFolder { get; }

        /// <summary>
        /// Returns System Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        string SystemUptime(int type);

        /// <summary>
        /// Returns AdiIRC Uptime in milliseconds
        /// if type = 1 returns a duration, if type = 2 returns a duration but without seconds, and if type = 3 returns seconds instead of milliseconds.
        /// </summary>
        string Uptime(int type);
    }
}
