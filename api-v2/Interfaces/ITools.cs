﻿namespace AdiIRCAPIv2
{
    using System;

    /// <summary>
    /// Various tools
    /// </summary>
    public interface ITools
    {
        /// <summary>
        /// Gets the bold character
        /// </summary>
        char BoldChar { get; }

        /// <summary>
        /// Gets the color character
        /// </summary>
        char ColorChar { get; }

        /// <summary>
        /// Gets the underline character
        /// </summary>
        char UnderLineChar { get; }

        /// <summary>
        /// Gets the action character
        /// </summary>
        char ActionChar { get; }

        /// <summary>
        /// Gets the nick column character used to determine where in the ITextView buffer the nick coulmn should be
        /// </summary>
        char NickColumnChar { get; }

        /// <summary>
        /// Gets the link character used to determine if the text in the ITextView buffer is a link
        /// </summary>
        char LinkChar { get; }

        /// <summary>
        /// Gets path sperator / or \ , for future use
        /// </summary>
        string PS { get; }

        /// <summary>
        /// Returns a string with colors stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripColors(string text);

        /// <summary>
        /// Returns a string with bold stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripBold(string text);

        /// <summary>
        /// Returns a string with italic stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripItalic(string text);

        /// <summary>
        /// Returns a string with underline stripped out
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>A stripped string</returns>
        string StripUnderline(string text);

        /// <summary>
        /// Use this to write to debug.txt in the main folder, returns true if the debug was written
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>True/False depending if the write was successful</returns>
        bool Debug(string text);

        /// <summary>
        /// Converts string to DateTime, see Time variables for possible syntax
        /// </summary>
        /// <param name="syntax">String</param>
        /// <param name="time">DateTime</param>
        /// <returns>A parsed string</returns>
        string ParseTime(string syntax, DateTime time);

        /// <summary>
        /// Converts unix timestamp to a DateTime
        /// </summary>
        /// <param name="time">Double</param>
        /// <returns>A Datetime value</returns>
        DateTime UnixTimeToDate(double time);

        /// <summary>
        /// Converts DateTime to unix timestamp
        /// </summary>
        /// <param name="time">DateTime</param>
        /// <returns>A unix timestamp value</returns>
        long DateToUnixTime(DateTime time);

        /// <summary>
        /// Checks if a string is numeric
        /// </summary>
        /// <param name="text">String</param>
        /// <returns>True/False whether the string is a number</returns>
        bool IsNumeric(string text);

        /// <summary>
        /// Open ups a tool window
        /// </summary>
        /// <param name="height">int</param>
        /// <param name="width">int</param>
        /// <param name="title">String</param>
        /// <param name="text">String</param>
        void ToolWindow(int height, int width, string title, string text);

        /// <summary>
        /// Makes the text bold if Boldify is enabled
        /// </summary>
        /// <param name="text">Text to boldify</param>
        /// <returns>Boldified text</returns>
        string Boldify(string text);

        /// <summary>
        /// Splits a string by a string
        /// </summary>
        /// <param name="text">String</param>
        /// <param name="splitBy">String</param>
        /// <returns>A list of strings</returns>
        string[] SplitString(string text, string splitBy);

        /// <summary>
        /// Returns system information based on Syntax, see system variables for more information
        /// </summary>
        /// <param name="text">String</param>
        /// <param name="args">String</param>
        /// <returns>A oarsed string</returns>
        string ParseSysinfo(string text, string args);
    }
}
